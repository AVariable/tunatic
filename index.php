<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Login</title>
	<link rel="stylesheet" type="text/css" href="CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="CSS/index.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			Tunatic
		</div>
		<div class="content">
			<p><h3>Bem-vindo ao Tunatic, onde tunas e utilizadores se encontram!</h3></p> 
	    </div>
	  	<div align="center" class="sidebar2">
	    	<form action="processar_login.php" method="post">
	    		<br />
	    		<h3>Login</h3>
    			<span>Email</span>
    			<br />
    			<input type="text" name="email" />
	    		<br />
	    		<span>Password</span>
	    		<br />
	    		<input type="password" name="password" />
	    		<br />
	    		<br />
	    		<input style="width: 120px;" class="login" value="Iniciar Sessão" type="submit" />
	    		<br />
	    		<h3>Registo</h3>
	    		<input style="width: 120px;" class="registarUtilizador" value="Registar Utilizador" type="Button" onclick="window.location.href='Registar/registoUtilizador.php'" />
	    		<br />
	    		<br />
	    		<input style="width: 120px;" class="registarTuna" value="Registar Tuna" type="Button" onclick="window.location.href='Registar/registoTuna.php'" />
	    	</form>
	    </div>
	    <?php
	    	include("footer.php");
	    ?>
    </div>
</body>
</html>
<?php
	session_start();
	// Se a sessão já existir redirecciona para a página de feed
	if(isset($_SESSION['idConta']))
	{
		header("Location: Pagina_Principal/Feed/feed.php");
	}
?>