<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Registo Tuna</title>
	<link rel="stylesheet" type="text/css" href="../CSS/default.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../index.php">Tunatic</a> 
		</div>
		<div class="content">
			<h1>Registo Tuna</h1>
			<form action="processar_registo_tuna.php" method="post" enctype="multipart/form-data">
    			<span>Email</span>
    			<br />
    			<input type="email" name="email" />
    			<br />
	    		<span>Password</span>
	    		<br />
	    		<input type="password" name="password" />
	    		<br />
	    		<span>Nome</span>
	    		<br />
	    		<input type="text" name="nome" />
	    		<br />
	    		<span>Fundada em</span>
	    		<br />
	    		<input type="date" name="dataNasc" />
	    		<br />
	    		<span>Sobre</span>
	    		<br />
	    		<textarea rows="5" cols="100" name="sobre" /></textarea>
	    		<br />
	    		<span>Designação</span>
	    		<br />
	    		<input type="text" name="designacao" />
	    		<br />
	    		<span>Tipo de Tuna</span>
	    		<br />
	    		<select name="tipo">
	    			<option value='0'>Masculina</option>
	    			<option value='1'>Feminina</option>
	    			<option value='2'>Mista</option>
	    		</select>
	    		<br />
	    		<input type="hidden" name="MAX_FILE_SIZE" value="1024000">
				<span>Foto de perfil</span>
				<br />
				<input type="file" name="origem" id="file">
	    		<br />
	    		<br />
	    		<input class="login" value="Registar" type="submit" />
	    		<input value="Voltar" type="Button" onclick="window.location.href='../index.php'" />
	    	</form>
	    </div>
	  	<?php
	    	include("../footer.php");
	    ?>
    </div>
</body>
</html>