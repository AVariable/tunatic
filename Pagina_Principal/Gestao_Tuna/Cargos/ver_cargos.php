<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Gerir Tuna</title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					if(!isset($_GET['idTuna']))
					{
						echo '<li><a href="../gerir_tuna.php">Gerir Tuna</a></li>';
						echo '<li><a href="../ver_membros.php">Gerir Membros</a></li>';
						echo '<li><a href="../Familia/ver_familia.php">Gerir Familia</a></li>';
						echo '<li><a href="../Cargos/ver_cargos.php">Gerir Cargos</a></li>';
						echo '<li><a href="../Geracoes/ver_geracoes.php">Gerir Gerações</a></li>';
						echo '<li><a href="../../Feed/feed.php">Voltar</a></li>';
					}
					else 
					{
						// Link para a página de perfil dependendo do tipo de conta que fez login
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo "<li><a href='../../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
						}
						// Se a sessão for do tipo Utilizador
						else 
						{
							echo "<li><a href='../../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						}
						// Link para a página de amigos dependendo do tipo de conta que fez login
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo "<li><a href='../../Amigos/amigos_tuna.php'>Amigos</a></li>";
						}
						// Se a sessão for do tipo Utilizador
						else 
						{
							echo "<li><a href='../../Amigos/amigos_utilizador.php'>Amigos</a></li>";
						}
						// Links para as páginas seguidores e membros de uma Tuna
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo'<li><a href="../../Festivais/festivais.php">Festivais</a></li>';
							echo"<li><a href='../../Seguidores/seguidores.php'>Seguidores</a></li>";
							echo"<li><a href='../../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
						}
					}
				?>
			</ul>
	    </div>
		<div class="content">
		    <h3>Gestão dos Cargos</h3>
		    	<?php
					include('../../../ligacao_bd.php');
					
					if(isset($_GET['idTuna']))
					{
						echo '<ul class="nav">';
						$idTuna = $_GET['idTuna'];
			    		$sql = 'SELECT idCargo, designacao FROM cargos WHERE idTuna='.$idTuna.' ORDER BY importancia DESC;';
			    		$result = mysql_query($sql, $link) or die(mysql_error($link));
			    		while($row = mysql_fetch_array($result)) 
			    		{
			    			echo '<li><a href="pagina_cargo.php?idCargo='.$row['idCargo'].'">'.$row['designacao'].'</a></li>';
			    		}
						echo '</ul>';
					}
					else 
					{
						echo '<ul class="nav">';
						$idTuna = $_SESSION['idTuna'];
			    		$sql = 'SELECT idCargo, designacao FROM cargos WHERE idTuna='.$idTuna.' ORDER BY importancia DESC;';
			    		$result = mysql_query($sql, $link) or die(mysql_error($link));
			    		while($row = mysql_fetch_array($result)) 
			    		{
			    			echo '<li><a href="pagina_cargo.php?idCargo='.$row['idCargo'].'">'.$row['designacao'].'</a></li>';
			    		}
						echo '</ul>';
						echo '<p><a href="adicionar_cargo.php">Novo Cargo</a> | <a href="associar_cargo.php">Associar Membros</a></p>';
					}
		    	?>
    			</ul>
	    </div>
	  	<?php
	  		
	  		include("../../../ligacao_bd.php");
	  		include("../sidebar3.php");
	    	include("../../../footer.php");
	    ?>
    </div>
</body>
</html>