<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Gerir Tuna</title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					include('../../../ligacao_bd.php');
					
					if($_SESSION['tipoTuna'])
					{
						$sql = 'SELECT * FROM cargos WHERE idCargo = ' . $_GET['idCargo'] . ' AND idTuna = ' . $_SESSION['idTuna'] . ';';
						$result = mysql_query($sql, $link) or die(mysql_error($link));
						if(mysql_num_rows($result) > 0)
						{
							echo '<li><a href="../gerir_tuna.php">Gerir Tuna</a></li>';
							echo '<li><a href="../ver_membros.php">Gerir Membros</a></li>';
							echo '<li><a href="../Familia/ver_familia.php">Gerir Familia</a></li>';
							echo '<li><a href="ver_cargos.php">Gerir Cargos</a></li>';
							echo '<li><a href="../Geracoes/ver_geracoes.php">Gerir Gerações</a></li>';
							echo '<li><a href="../../Feed/feed.php">Voltar</a></li>';
						}
						else 
						{
							echo "<li><a href='../../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
							echo "<li><a href='../../Amigos/amigos_tuna.php'>Amigos</a></li>";
							echo'<li><a href="../../Festivais/festivais.php">Festivais</a></li>';
							echo"<li><a href='../../Seguidores/seguidores.php'>Seguidores</a></li>";
							echo"<li><a href='../../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
						}
					}
					else 
					{
						echo "<li><a href='../../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						echo "<li><a href='../../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
	    	<?php
				$idCargo=$_GET['idCargo'];
				
				//$idTuna = $_SESSION['idTuna'];
	    		$sql = 'SELECT * FROM posses_cargo 
	    		INNER JOIN cargos USING (idCargo) 
	    		WHERE idCargo='.$idCargo.' ORDER BY data DESC LIMIT 1;';
	    		$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				if(mysql_num_rows($result)>0)
				{
		    		$sql = 'SELECT * FROM membros WHERE idMembro='.$row['idMembro'].';';
		    		$resultMembro = mysql_query($sql, $link) or die(mysql_error($link));
		    		$rowMembro = mysql_fetch_array($resultMembro);	
				
	    			echo '<h1 style="padding-left: 15px;">' . $row['designacao'] . '</h1>';	
	    			echo '<div style="padding: 10px 0; width: 33%; float: left;">';
					echo '<span style="padding-left: 15px;"><img src="../../../' . $rowMembro['img_path'] . '" width="200" height="200"></span>';
				    echo '</div>';
				    echo '<div style="padding: 10px 0; width: 33%; float: left;">';
				    echo '<p><a href="../../perfil/perfil_membro_tuna.php?idMembro=' . $rowMembro['idMembro'] . '" style="font-size: 50px;">' . $rowMembro['nome_tuna'] . '</a></p>';
				    echo '<p>Actual ' . $row['designacao'] . '</p>';
				    echo '<p>Posse em: ' . $row['data'] . '</p>';
					echo '</div>';
					echo '<div style="padding: 10px 0; width: 33%; float: left; text-align: center;">';
		    		if($_SESSION['tipoTuna']){
			    		if($row['idTuna']==$_SESSION['idTuna']){
			    			echo '<a href = "processar_remover_cargo.php?idCargo='.$idCargo.'"><span style="color:red;">Eliminar Cargo</span></a>';
			    		}
					}
				    echo '</div>';
				    echo '<div style="padding: 10px 0; width: 100%; float: left;">';
			    	echo '<h3>Anteriores Detentores</h3>';
				    echo '<ul class="nav">';
					
		    		$sql = 'SELECT * FROM posses_cargo 
		    		INNER JOIN cargos USING (idCargo) 
		    		WHERE idCargo='.$idCargo.' ORDER BY data DESC;';
		    		$result = mysql_query($sql, $link) or die(mysql_error($link));
					
					if(mysql_num_rows($result)>0)
					{
						$count=0;
						while($row = mysql_fetch_array($result))
						{
							$count++;
							if($count != 1)
							{
								$sql = 'SELECT * FROM membros WHERE idMembro='.$row['idMembro'].';';
			    				$resultMembro = mysql_query($sql, $link) or die(mysql_error($link));
			    				$rowMembro = mysql_fetch_array($resultMembro);
								echo'<li><a href="../../Perfil/perfil_membro_tuna.php?idMembro='.$rowMembro['idMembro'].'"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../../'.$rowMembro['img_path'].'" width="50" height="50"></span>'.$rowMembro['nome_tuna'].' - Posse em: '.$row['data'].'</a></li>';	
							}
						}
					}
					
					echo '</ul>';
					echo '</div>';
			    } 
			    else
			    {
			    	$sql = 'SELECT * FROM tunas 
			    			INNER JOIN cargos USING (idTuna)
			    			WHERE idCargo = ' . $idCargo . ';';
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					$row = mysql_fetch_array($result);
			    	echo '<h2>Cargo sem membro associado...</h2>';
					if($_SESSION['tipoTuna']){
			    		if($row['idTuna']==$_SESSION['idTuna']){
			    			echo '<p><a href = "processar_remover_cargo.php?idCargo='.$idCargo.'"><span style="color:red;">Eliminar Cargo</span></a></p>';
			    		}
					}
			    }
			?>
	    </div>
	  	<?php
	  		include("../../../ligacao_bd.php");
	  		include("../sidebar3.php");
	    	include("../../../footer.php");
	    ?>
    </div>
</body>
</html>