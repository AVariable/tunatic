<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Gerir Tuna</title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					if(!isset($_GET['idTuna']))
					{
						echo '<li><a href="../gerir_tuna.php">Gerir Tuna</a></li>';
						echo '<li><a href="../ver_membros.php">Gerir Membros</a></li>';
						echo '<li><a href="../Familia/ver_familia.php">Gerir Familia</a></li>';
						echo '<li><a href="../Cargos/ver_cargos.php">Gerir Cargos</a></li>';
						echo '<li><a href="../Geracoes/ver_geracoes.php">Gerir Gerações</a></li>';
						echo '<li><a href="../../Feed/feed.php">Voltar</a></li>';
					}
					else 
					{
						// Link para a página de perfil dependendo do tipo de conta que fez login
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo "<li><a href='../../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
						}
						// Se a sessão for do tipo Utilizador
						else 
						{
							echo "<li><a href='../../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						}
						// Link para a página de amigos dependendo do tipo de conta que fez login
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo "<li><a href='../../Amigos/amigos_tuna.php'>Amigos</a></li>";
						}
						// Se a sessão for do tipo Utilizador
						else 
						{
							echo "<li><a href='../../Amigos/amigos_utilizador.php'>Amigos</a></li>";
						}
						// Links para as páginas seguidores e membros de uma Tuna
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo'<li><a href="../../Festivais/festivais.php">Festivais</a></li>';
							echo"<li><a href='../../Seguidores/seguidores.php'>Seguidores</a></li>";
							echo"<li><a href='../../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
						}
					}
				?>
			</ul>
	    </div>
		<div class="content">
		    <h3>Gerações</h3>
			    <?php
					include('../../../ligacao_bd.php');
					
					if(isset($_GET['idTuna']))
					{
						$idTuna = $_GET['idTuna'];
					
						$sql = 'SELECT * FROM geracoes WHERE idTuna = ' . $idTuna . ' ORDER BY numero ASC;';
						$resultg = mysql_query($sql, $link) or die(mysql_error($link));
						
						while($rowg = mysql_fetch_array($resultg)) 
						{		
						    $sql = 'SELECT *, membros.img_path AS imgm from membros
						    		INNER JOIN tunas USING (idTuna)
						    		WHERE idGeracao = '.$rowg['idGeracao'].';';
							$result = mysql_query($sql, $link) or die(mysql_error($link));
							
							if($rowg['numero'] == 0)
							{
								echo '<span style="padding-left:15px; padding-bottom:5px; font-size:20px;">Fundadores ('.$rowg['ano'].')</span></a>';
							}
							else 
							{
								echo '<span style="padding-left:15px; padding-bottom:5px; font-size:20px;">'.$rowg['numero'].'ª Geração ('.$rowg['ano'].')</span></a>';
							}
							echo '<ul class="nav">';
							while($row= mysql_fetch_array($result)) 
							{
								echo'<li><a href="../../Perfil/perfil_membro_tuna.php?idMembro='.$row['idMembro'].'"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../../'.$row['imgm'].'" width="50" height="50"></span>'.$row['nome_tuna'].'</a></li>';	
							}
							echo '</ul>';
						}
					}
					else 
					{
						$idTuna = $_SESSION['idTuna'];
					
						$sql = 'SELECT * FROM geracoes WHERE idTuna = '.$idTuna.' ORDER BY numero ASC;';
						$resultg = mysql_query($sql, $link) or die(mysql_error($link));
						
						while($rowg = mysql_fetch_array($resultg)) 
						{		
						    $sql = 'SELECT *, membros.img_path AS imgm from membros
						    		INNER JOIN tunas USING (idTuna)
						    		WHERE idGeracao = '.$rowg['idGeracao'].';';
							$result = mysql_query($sql, $link) or die(mysql_error($link));
							
							if($rowg['numero'] == 0)
							{
								echo '<span style="padding-left:15px; padding-bottom:5px; font-size:20px;">Fundadores ('.$rowg['ano'].')</span><a href="processar_remover_geracao.php?idGeracao=' . $rowg['idGeracao'] . '"><span style="padding-left: 15px; color: red;">Eliminar</span></a>';
							}
							else 
							{
								echo '<span style="padding-left:15px; padding-bottom:5px; font-size:20px;">'.$rowg['numero'].'ª Geração ('.$rowg['ano'].')</span><a href="processar_remover_geracao.php?idGeracao=' . $rowg['idGeracao'] . '"><span style="padding-left: 15px; color: red;">Eliminar</span></a>';
							}
							echo '<ul class="nav">';
							while($row= mysql_fetch_array($result)) 
							{
								echo'<li><a href="../../Perfil/perfil_membro_tuna.php?idMembro='.$row['idMembro'].'"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../../'.$row['imgm'].'" width="50" height="50"></span>'.$row['nome_tuna'].'</a></li>';	
							}
							echo '</ul>';
						}
						echo '<p><a href="adicionar_geracao.php">Nova Geração</a> | <a href="associar_geracao.php">Associar Membros</a></p>';
					}
			    ?>
	    </div>
	  	<?php
	  		mysql_close($link);
	  		include("../../../ligacao_bd.php");
	  		include("../sidebar3.php");
	    	include("../../../footer.php");
	    ?>
    </div>
</body>
</html>