<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Gerir Tuna</title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<li><a href="../gerir_tuna.php">Gerir Tuna</a></li>
				<li><a href="../ver_membros.php">Gerir Membros</a></li>
				<li><a href="../familia/ver_familia.php">Gerir Familia</a></li>
				<li><a href="../Cargos/ver_cargos.php">Gerir Cargos</a></li>
				<li><a href="ver_geracoes.php">Gerir Gerações</a></li>
				<li><a href="../../Feed/feed.php">Voltar</a></li>
			</ul>
	    </div>
		<div class="content">
		    <h3>Associar Membros a Gerações</h3>
		    <?php
		    session_start();
			include('../../../ligacao_bd.php');
			$idTuna = $_SESSION['idTuna'];
			$sql = 'SELECT * FROM geracoes WHERE idTuna='.$idTuna.' ORDER BY numero ASC;';
			$result = mysql_query($sql, $link) or die(mysql_error($link));
			while($row = mysql_fetch_array($result)) {
				
				$idGeracao = $row['idGeracao'];
				
				echo '<hr><span style="font-size: 18px; padding-left:15px;">'.$row['numero'].'ª Geração ('.$row['ano'].')</span>
				<form action="processar_associacao.php" method="post">	
				<span style="padding-left:15px;">
				Associar membro: <select name="membro"> ';
				
				$sql = 'SELECT * FROM membros WHERE idTuna = '.$idTuna.' AND idGeracao IS NULL;';
				$result2 = mysql_query($sql, $link) or die(mysql_error($link));
				while($row2 = mysql_fetch_array($result2)) {
					
					
					echo '<option value="'.$row2['idMembro'].'">'.$row2['nome_tuna'].'</option>';
					
				}
				echo '</select></span>
				<input type="hidden" name="idGeracao" value="'.$idGeracao.'">
				<input type="submit" value="Associar"/>
				</form>';
			}
			
		    ?>
		    <hr>	
	    </div>
	  	<?php
	  		include("../../../ligacao_bd.php");
	  		include("../sidebar3.php");
	    	include("../../../footer.php");
	    ?>
    </div>
</body>
</html>