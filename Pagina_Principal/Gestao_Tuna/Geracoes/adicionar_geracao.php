<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Gerir Tuna</title>
	<link rel="stylesheet" type="text/css" href="../../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<li><a href="../gerir_tuna.php">Gerir Tuna</a></li>
				<li><a href="../ver_membros.php">Gerir Membros</a></li>
				<li><a href="ver_familia.php">Gerir Familia</a></li>
				<li><a href="../Cargos/ver_cargos.php">Gerir Cargos</a></li>
				<li><a href="ver_geracoes.php">Gerir Gerações</a></li>
				<li><a href="../../Feed/feed.php">Voltar</a></li>
			</ul>
	    </div>
		<div class="content">
		    <h3>Nova Geração</h3>
		    
		    <form action="processar_adicionar_geracao.php" method="post">
		    	<p>Numero da geração:<br />
		    	<input type="number" name="numero" />
		    	</p>
			    <p>Ano:<br />
		    	<input type="number" name="ano" />
		    	</p>
		    	<p><input type="submit" value="Adicionar" /></p>
		    </form>
	    </div>
	  	<?php
	  		session_start();
	  		include("../../../ligacao_bd.php");
	  		include("../sidebar3.php");
	    	include("../../../footer.php");
	    ?>
    </div>
</body>
</html>