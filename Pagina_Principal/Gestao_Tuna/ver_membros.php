<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Amigos</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					if(!isset($_GET['idTuna']))
					{
						echo '<li><a href="gerir_tuna.php">Gerir Tuna</a></li>';
						echo '<li><a href="ver_membros.php">Gerir Membros</a></li>';
						echo '<li><a href="Familia/ver_familia.php">Gerir Familia</a></li>';
						echo '<li><a href="Cargos/ver_cargos.php">Gerir Cargos</a></li>';
						echo '<li><a href="Geracoes/ver_geracoes.php">Gerir Gerações</a></li>';
						echo '<li><a href="../Feed/feed.php">Voltar</a></li>';
					}
					else 
					{
						// Link para a página de perfil dependendo do tipo de conta que fez login
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
						}
						// Se a sessão for do tipo Utilizador
						else 
						{
							echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						}
						// Link para a página de amigos dependendo do tipo de conta que fez login
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
						}
						// Se a sessão for do tipo Utilizador
						else 
						{
							echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
						}
						// Links para as páginas seguidores e membros de uma Tuna
						// Se a sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
							echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
							echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
						}
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<?php
				include('../../ligacao_bd.php');
				
				// Se a Tuna que vem em GET não existir
				if(!isset($_GET['idTuna']))
				{
					echo '<h3>Os meus Membros</h3>';
				}
				// Se a Tuna que vem em GET existir
				else 
				{
					$sql = 'SELECT * FROM Contas
							INNER JOIN tunas USING (idConta) 
							WHERE idTuna = ' . $_GET['idTuna'] . ';';
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					$row = mysql_fetch_array($result);
					echo '<h3>Membros de ' . $row['nome'] . '</h3>';
				}
			?>
			<ul class="men">
			<?php
				//Se a Tuna que vem em GET não existir
				if(!isset($_GET['idTuna']))
				{
					// Query que selecciona toda a informação referentes aos Membros da Tuna em sessão
					$sql = 'SELECT *, membros.img_path AS imgMembro, 
									  membros.nome_tuna AS nomeMembro
							FROM contas
							INNER JOIN utilizadores USING (idConta)
							INNER JOIN membros USING (idUtilizador)
							WHERE idTuna = ' . $_SESSION['idTuna'];
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					while($row = mysql_fetch_array($result))
					{
						echo '<li><a href="editar_perfil_membro.php?idMembro=' . $row['idMembro'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../'.$row['img_path'].'" width="50" height="50"></span>'.$row['nomeMembro'].'
							<span style="padding-left:25px; font-size:12px;">Editar Perfil</span></a>
							</li>';
					}
				}
				// Se a Tuna que vem em GET existir
				else
				{
					// Query que selecciona toda a informação referentes aos Membros da Tuna que vem em GET
					$sql = 'SELECT *, membros.img_path AS imgMembro, 
									  membros.nome_tuna AS nomeMembro
							FROM contas
							INNER JOIN utilizadores USING (idConta)
							INNER JOIN membros USING (idUtilizador)
							WHERE idTuna = ' . $_GET['idTuna'];
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					while($row = mysql_fetch_array($result))
					{
						echo '<li><a href="../Perfil/perfil_membro_tuna.php?idMembro=' . $row['idMembro'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../'.$row['img_path'].'" width="50" height="50"></span>'.$row['nomeMembro'].'
							</a>
							</li>';
					}
				}
			?>
			</ul>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>