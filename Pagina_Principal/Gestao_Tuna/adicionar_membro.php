<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Feed</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			Tunatic
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>
				<li><a href="../Mensagens/mensagens.php">Mensagens</a></li>
				<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>
				<li><a href="../Festivais/festivais.php">Festivais</a></li>
				<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>
				<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>
				<li><a href="../processar_logout.php">Logout</a></li>
			</ul>
	    </div>
		<div align="center" class="content">
		    <h1>Adicionar Membro</h1>
		    <form action="processar_adicionar_membro.php" method="post" enctype="multipart/form-data">
		    	<input type="hidden" value="<?php echo $_GET['idUtilizador']; ?>" name="idUtilizador"/>
				Nome em Tuna<br /><input type="text"/ name="nome"><br /><br />
				Tipo<br /><input type="text" name="tipo" /><br /><br />
				Instrumento<br /><input type="text" name="instrumento" /><br /><br />
				Data de Entrada<br /><input type="date" name="data" /><br /><br />
				Sobre<br /><textarea rows="5" cols="50" name="sobre" /></textarea><br /><br />
				<input type="hidden" name="MAX_FILE_SIZE" value="1024000">
				<input type="file" name="origem" id="file">
				<br />
				<br />
				<input type="submit" value="Adicionar Membro" />
			</form>
	    </div>
	  	<?php
	  		include("../../ligacao_bd.php");
	  		session_start();
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>