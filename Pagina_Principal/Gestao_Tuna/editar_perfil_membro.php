<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Membro</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
	  		<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div align="center" class="content">
			<h1>Gestão de Conta</h1>
			<form action="processar_alterar_membro.php" method="post" enctype="multipart/form-data">
			<?php
				// Query que selecciona toda a informação referente à Conta em sessão
				$sql = 'SELECT *, membros.img_path AS imgMembro, 
								  membros.nome_tuna AS nomeMembro,
								  membros.sobre AS sobreMembro
							FROM contas
							INNER JOIN utilizadores USING (idConta)
							INNER JOIN membros USING (idUtilizador)
							WHERE idMembro = ' . $_GET['idMembro'] . ';';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				
				echo '<input type="hidden" value="' . $_GET['idMembro'] . '" name="idMembro" />';
				echo'Nome<br /> <input type="text" value="' . $row['nome_tuna'] . '" name="nome"/><br /><br />';
				echo'Tipo de Membro<br /> <input type="text" value="' . $row['tipo'] . '" name="tipo"/><br /><br />';
				echo'Membro desde<br /> <input type="date" value="' . $row['data_entrada'] . '" name="data" /><br /><br />';
				echo'Instrumento<br /> <input type="text" value="' . $row['instrumento'] . '" name="instrumento" /><br /><br />';
				echo'Sobre<br /> <textarea rows="5" name="sobre">' . $row['sobreMembro'] . '</textarea><br /><br />';
				echo 'Membro ativo<br /> <select name="ativo">';
				if($row['ativo'] == 1)
				{
					echo '<option value="1">Ativo</option>';
					echo '<option value="0">Inativo</option>';
				}
				else
				{
					echo '<option value="0">Inativo</option>';
					echo '<option value="1">Ativo</option>';
				}
				echo '</select><br /><br />';
			?>
			<img src="../../<?php echo $row['imgMembro']; ?>" width="200" height="200">
			<br />
			<input type="hidden" name="MAX_FILE_SIZE" value="1024000">
			<input type="file" name="origem" id="file">
			<br />
			<br />
			<input type="submit" value="Guardar Alterações" />
			</form>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>