<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Ver Mensagem</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<br />
			<?php
				// Query que verifica se a mensagem que veio em GET já foi lida
				$sql = 'SELECT * FROM mensagem_receptores WHERE idMensagem = ' . $_GET['idMensagem'] . ' AND idConta = ' . $_SESSION['idConta'] . ' AND lida = 1;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				
				// Se a mensagem não estiver lida
				if(mysql_num_rows($result) == 0)
				{
					// Query que torna a mensagem lida 
					$sql = 'UPDATE mensagem_receptores SET lida = 1 WHERE idMensagem = ' . $_GET['idMensagem'] . ' AND idConta = ' . $_SESSION['idConta'] . ';';
					mysql_query($sql, $link) or die(mysql_error($link));
				}
				
				// Query que selecciona a informação da Mensagem que veio em GET
				$sql = "SELECT * FROM mensagens WHERE idMensagem = " . $_GET['idMensagem'] . ";";
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while($row = mysql_fetch_array($result))
				{
					// Query que selecciona a Conta que enviou a mensagem
					$sql = "SELECT * FROM contas WHERE idConta = " . $row['idConta'] . ";";
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					$row2 = mysql_fetch_array($result2);
					
					echo "<p>Remetente: " . $row2['nome'] . "</p>";
					echo "<p>Assunto: " . $row['assunto'] . " | " . $row['timestamp'] . "</p>";
					echo "<p><textarea rows='10' cols='50' disabled='true'>" . $row['texto'] . "</textarea></p>";
					echo '<p>';
					echo '<a href="criar_mensagem_unica.php?idConta=' . $row2['idConta'] . '">Responder</a> ';
					echo '<a href="processar_remover_mensagem.php?idMensagem=' . $row['idMensagem'] . '"><font color="FF0000">Apagar Mensagem</font></a>';
					echo '</p>'; 
				}
			?>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>