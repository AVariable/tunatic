<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Mensagens</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<h3>Mensagens</h3>
			<p><a href="criar_mensagem.php">Criar Mensagem</a></p>
			<ul class="men">
			<?php
				// Query que selecciona todas as mensagens recebidas da Conta em sessão
				$sql = "SELECT *, mensagens.idConta AS 'mid' FROM mensagens
						INNER JOIN mensagem_receptores USING (idMensagem)
						WHERE mensagem_receptores.idConta = '" . $_SESSION['idConta'] . "'
						ORDER BY mensagens.timestamp DESC;";
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while($row = mysql_fetch_array($result))
				{
					// Query que selecciona a Conta que mandou mensagem à Conta em sessão
					$sql = "SELECT * FROM contas WHERE idConta = '" . $row['mid'] . "';";
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					$row2 = mysql_fetch_array($result2);
					echo "<li>";
					echo "<a href='ver_mensagem.php?idMensagem=" . $row['idMensagem'] . "'>";
					echo $row2['nome'] . " | ";
					echo $row['assunto'] . " | ";
					echo $row['timestamp'];
					echo "</a>";
					echo "</li>";
				}
			?>
			</ul>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>