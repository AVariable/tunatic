<?php
	session_start();
	
	// Se existirem destinatários
	if(!empty($_POST['destinatarios']))
	{
		include('../../ligacao_bd.php');
		
		// Query que cria uma nova mensagem pela Conta em sessão
		$sql = "INSERT INTO mensagens (texto, timestamp, assunto, idConta)
				VALUES('" . $_POST['texto'] . "',
					   	   now(), '"
					 	 . $_POST['assunto'] . "', "
					 	 . $_SESSION['idConta'] . ")";
		$resultado = mysql_query($sql, $link) or die(mysql_error($link));
		
		// Se a criação da mensagem foi bem sucedida
		if ($resultado !== 1)
		{
			// Query que selecciona a ultima Mensagem criada pelo Utilizador em sessão
			$sql = "SELECT MAX(idMensagem) as idMensagem FROM mensagens WHERE idConta = " . $_SESSION['idConta'] . ";";
			$result = mysql_query($sql, $link) or die(mysql_error($link));
			$row = mysql_fetch_array($result);
			
			// Para cada destinatário escolhido na criação da mensagem
			foreach($_POST['destinatarios'] as $key => $value)
			{
				// Query que insere o destinatario na mensagem
				$sql = "INSERT INTO mensagem_receptores VALUES (" . $row['idMensagem'] . ", " . $value . ", 0)";
				$resultado = mysql_query($sql, $link) or die(mysql_error($link));
			}
		}
		
		mysql_close($link);
		header("Location: mensagens.php");
	}
	// Se não existirem destinatários
	else
	{
		echo ('<script type="text/javascript">'); 
		echo ('alert("Mensagem sem destinatários. Mensagem não enviada.");'); 
		echo ('window.location.replace("mensagens.php");');
		echo ('</script>');
	}
?>