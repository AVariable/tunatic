<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Criar Mensagem</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div align="center" class="content">
			<h2>Criar Mensagem</h2>
			<br />
			<form action="processar_nova_mensagem.php" method="post">
				Assunto
				<br />
				<input size="78" type="text" name="assunto" />
				<br />
				Texto
				<br />
				<textarea cols="60" rows="8" name="texto"></textarea>
				<br />
				<select multiple="multiple" name="destinatarios[ ]">
					<?php
						// Se a Conta em sessão for do tipo Tuna
						if($_SESSION['tipoTuna'])
						{
							// Query que selecciona todos os amigos da Tuna em sessão
							$sql = "SELECT * FROM amigos_tuna WHERE idTuna = " . $_SESSION['idTuna'] . " AND pedido_aceite = 1;";
							$result = mysql_query($sql, $link) or die(mysql_error($link));
							while($row = mysql_fetch_array($result))
							{
								// Query que selecciona a informação relativa à Conta e à Tuna da linha actual
								$sql = "SELECT * FROM contas
										INNER JOIN tunas USING (idConta)
										WHERE idTuna = " . $row['idTuna2'] . ";";
								$result2 = mysql_query($sql, $link) or die(mysql_error($link));
								$row2 = mysql_fetch_array($result2);
								echo "<option value='" . $row2['idConta'] . "'>" . $row2['nome'] . "</option>";
							}
						}
						// Se a Conta em sessão for do tipo Utilizador
						else
						{
							// Query que selecciona todos os amigos do Utilizador em sessão
							$sql = "SELECT * FROM amigos_utilizador WHERE idUtilizador = " . $_SESSION['idUtilizador'] . " AND pedido_aceite = 1;";
							$result = mysql_query($sql, $link) or die(mysql_error($link));
							while($row = mysql_fetch_array($result))
							{
								// Query que selecciona a informação relativa à Conta e ao Utilizador da linha actual
								$sql = "SELECT * FROM contas
										INNER JOIN utilizadores USING (idConta)
										WHERE utilizadores.idUtilizador = " . $row['idUtilizador2'] . ";";
								$result2 = mysql_query($sql, $link) or die(mysql_error($link));
								$row2 = mysql_fetch_array($result2);
								echo "<option value='" . $row2['idConta'] . "'>" . $row2['nome'] . "</option>";
							}	 	
						}
					?>
				</select>
				<br />
				<br />
				<input style="width: 100px;" type="submit" value="Enviar" /> 
			</form>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>