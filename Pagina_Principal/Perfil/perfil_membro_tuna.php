<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Perfil</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
	  		<ul class="nav">
	  			<?php
	  				session_start();
	  				// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
	  				if($_SESSION['tipoTuna'])
					{
						$href = 'meu_perfil_tuna.php';
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						$href = 'meu_perfil_utilizador.php';
					}
	  			?>
				<li><a href="<?php echo $href;?>">Perfil</a></li>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<?php
				// Query que selecciona toda a informação referente ao Utilizador que é Membro
				$sql = 'SELECT *, utilizadores.idConta AS idcontaUtilizador, 
								  membros.img_path AS imgMembro, 
								  tunas.img_path AS imgTuna, 
								  membros.tipo AS tipoMembro
						FROM contas
						INNER JOIN utilizadores USING (idConta)
						INNER JOIN membros USING (idUtilizador)
						INNER JOIN tunas USING (idTuna)
						WHERE idMembro = ' . $_GET['idMembro'] . ';';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				// Query que selecciona toda a informação referente à Tuna que é do Membro
				$sql = 'SELECT * FROM contas
						INNER JOIN tunas USING (idConta)
						INNER JOIN membros USING (idTuna)
						INNER JOIN utilizadores USING (idUtilizador)
						WHERE idMembro = ' . $_GET['idMembro'] . ';';
				$result2 = mysql_query($sql, $link) or die(mysql_error($link));
				$row2 = mysql_fetch_array($result2);
			?>
			<h2>Perfil | <?php echo $row['nome_tuna'];?></h2>
		    <div style="text-align: center;" class="subcontent_top">
		    	<p><img src="../../<?php echo $row['imgMembro'] ?>" width="150" height="150"></p>
		    	<p>Membro de:
		    	<?php
		    		// Se a sessão for do tipo Tuna 
		    		if(isset($_SESSION['idTuna']))
					{
			    		if($_SESSION['idTuna'] == $row2['idTuna'])
						{
							$href = '../Perfil/meu_perfil_tuna.php';
						} 
						else
						{
							$href = '../Perfil/perfil_tuna.php?idTuna=' . $row2['idTuna'];
						}
					}
					// Se a sessão não for do tipo Tuna
					else
					{
						$href = '../Perfil/perfil_tuna.php?idTuna=' . $row2['idTuna'];
					}
		    	?>
		    	<a href="<?php echo $href;?>"><img src="../../<?php echo $row['imgTuna'] ?>" width="150" height="150">
		    	<?php echo $row2['nome']?></a></p>
		    </div>
		    <div class="subcontent_top_center" align="center">
		    	<?php
		    		// Se a sessão for do tipo Utilizador
		    		if(isset($_SESSION['idUtilizador']))
					{
			    		if($_SESSION['idUtilizador'] == $row['idUtilizador'])
						{
							$href = '../Perfil/meu_perfil_utilizador.php';
						} 
						else
						{
							$href = '../Perfil/perfil_utilizador.php?idUtilizador=' . $row['idUtilizador'];
						}
					}
					// Se a sessão não for do tipo Utilizador
					else
					{
						$href = '../Perfil/perfil_utilizador.php?idUtilizador=' . $row['idUtilizador'];
					}
		    	?>
		    	<p>Nome: <a href="<?php echo $href; ?>"><?php echo $row['nome'] ?></a></p>
		    	<p>Nome de tuna: <?php echo $row['nome_tuna'];?></p>
		    	<p>Tipo de Membro: <?php echo $row['tipoMembro'];?></p>
		    	<p>Membro desde: <?php echo $row['data_entrada'];?></p>
		    	<p>Instrumento: <?php echo $row['instrumento'];?></p>
		    	<?php
		    		// Query que selecciona o ultimo cargo do Membro referente à Tuna em que é Membro
		    		$sql = 'SELECT * FROM cargos;';
					$result3 = mysql_query($sql, $link) or die(mysql_error($link));
					while($row3 = mysql_fetch_array($result3))
					{
						$sql = 'SELECT * FROM posses_cargo WHERE idCargo = '.$row3['idCargo'].' ORDER BY data DESC LIMIT 1;';
						$result4 = mysql_query($sql, $link) or die(mysql_error($link));
						$row4 = mysql_fetch_array($result4);
						// Se existir algum cargo 
						if(mysql_num_rows($result4) > 0 && $row4['idMembro']== $_GET['idMembro'])
						{
							echo '<p>Cargo: <a href="../Gestao_Tuna/Cargos/pagina_cargo.php?idCargo='.$row3['idCargo'].'">' . $row3['designacao'] . '</a></p>';
						}
					}
		    	?>
		    </div>
		   	<div class="subcontent_top">
		   		<?php
		   			// Se a Conta que em sessão é do tipo Utilizador
		   			if(isset($_SESSION['idUtilizador']))
					{
						// Se o id do Utilizador em sessão for diferente do id do Utilizador na linha
			    		if($_SESSION['idUtilizador'] != $row['idUtilizador'])
						{
							echo '<p><a href="../Mensagens/criar_mensagem_unica.php?idConta=' . $row['idcontaUtilizador'] . '">Enviar Mensagem</a></p>';
						} 
					}
					// Se a Conta que em sessão é do tipo Tuna
					else
					{
						$sql = "SELECT * FROM membros WHERE idUtilizador = " . $row['idUtilizador'] . " AND idTuna = " . $_SESSION['idTuna'];
						$result = mysql_query($sql, $link) or die(mysql_error($link));
						if(mysql_num_rows($result) > 0)
						{
							echo "<p><a href='../Gestao_Tuna/remover_membro.php?idUtilizador=" . $row['idUtilizador'] . "'>Remover Membro</a></p>";
						}
						echo '<p><a href="../Mensagens/criar_mensagem_unica.php?idConta=' . $row['idcontaUtilizador'] . '">Enviar Mensagem</a></p>';
					}
		   		?>
		   	</div>
		   	<div class="subcontent_bottom">
			    <p>Sobre:</p>
			    <p><textarea disabled="true" rows="10" style="width: 98%;"><?php echo $row['sobre'] ?></textarea></p>
			</div>
	    </div>
	    <?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>