<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Perfil</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
	  		<ul class="nav">
				<li><a href="#">Perfil</a></li>
				<?php
					session_start();
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<?php
				// Query que selecciona toda a informação sobre a Conta e Tuna que está em sessão
				$sql = "SELECT * FROM contas 
						INNER JOIN tunas USING (idConta)
						WHERE idConta = '" . $_SESSION['idConta'] . "';";
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
			?>
			<h2>Perfil | 
				<?php 
					echo $row['nome'];
				?>
			</h2>
		    <div class="subcontent_top">
		    	<p><img src="../../<?php echo $row['img_path']; ?>" width="150" height="150"></p>
		    </div>
		    <div align="center" class="subcontent_top_center">
		    	<p>Nome: 
		    		<?php 
		    			echo $row['nome'];
	    			?>
	    		</p>
		    	<p>Designação:<br /> 
		    		<?php 
		    			echo $row['designacao'];
		    		?>
		    	</p>
		    	<p>Tipo de Tuna: 
		    		<?php
						switch($row['tipo'])
						{
							case 0:
								echo "Masculina";
								break;
							case 1:
								echo "Feminina";
								break;
							case 2:
								echo "Mista";
								break;
						} 
					?>
				</p>
		    	<p>Fundada em: 
		    		<?php 
		    			echo $row['data_nascimento'];
		    		?>
		    	</p>
		    	<p>Email: 
		    		<?php 
		    			echo $row['email'];
		    		?>
		    	</p>
		    	<p>Seguidores: 
		    		<?php 
		    			// Query que conta quantos seguidores a Tuna em sessão tem
						$sql = "SELECT COUNT(*) as nSeguidores FROM seguidores WHERE idTuna = " . $_SESSION['idTuna'];
						$resultSeguidores = mysql_query($sql, $link) or die(mysql_error($link));
						$rowSeguidores = mysql_fetch_array($resultSeguidores);
						echo $rowSeguidores['nSeguidores'];
					?>
				</p>
		    	<p>Tunas Amigas: 
					<?php 
		    			// Query que conta quantos amigos a Tuna em sessão tem
						$sql = 'SELECT COUNT(*) as nAmigos FROM amigos_tuna 
				   				INNER JOIN tunas USING (idTuna)
				   				WHERE idConta = ' . $_SESSION['idConta'] . ' AND pedido_aceite = 1;';
						$resultAmigos = mysql_query($sql, $link) or die(mysql_error($link));
						$rowAmigos = mysql_fetch_array($resultAmigos);
						echo $rowAmigos['nAmigos'];
					?>
					<a href="../Amigos/amigos_tuna.php">Ver Tunas Amigas</a>
				</p>
		    </div>
		   	<div align="center" class="subcontent_top">
		   		<p><a href="../Conta/conta.php">Editar Perfil</a></p>
		   	</div>
		   	<div class="subcontent_bottom">
		    	<p>Sobre a Tuna:</p>
		    	<p>
		    		<textarea disabled="true" rows="10" style="width: 98%;"><?php echo $row['sobre']; ?></textarea>
		    	</p>
		    	<div style="padding: 10px 0; width: 33%; float: left; text-align: center;">
		    		<?php
	    				// Query que conta quantos membros existem na Tuna em sessão
	    				$sql = "SELECT COUNT(*) as nMembros FROM membros WHERE idTuna = " . $_SESSION['idTuna'] . ";";	
						$resultnMembros = mysql_query($sql, $link) or die(mysql_error($link));		
						$rownMembros = mysql_fetch_array($resultnMembros);	
						if($rownMembros['nMembros'] > 0)
						{
							echo '<p>Membros: ';
							echo $rownMembros['nMembros'];
							echo '<br />';
							echo '<a href="../Gestao_Tuna/ver_membros.php">Ver Membros</a>';
						}
						else
						{
							echo '<br />';
							echo 'Não tem Membros.';
							echo '</p>';
						}
	    			?>
		   		</div>
		   		<div style="padding: 10px 0; width: 33%; float: left; text-align: center;">
		   			<?php
		   			// Query que conta quantas Gerações existem na Tuna em sessão
	    				$sql = "SELECT COUNT(*) as nGeracoes FROM geracoes WHERE idTuna = " . $_SESSION['idTuna'] . ";";	
						$resultnGeracoes = mysql_query($sql, $link) or die(mysql_error($link));		
						$rownGeracoes = mysql_fetch_array($resultnGeracoes);	
						if($rownGeracoes['nGeracoes'] > 0)
						{
							echo '<p>Gerações: ';
							echo $rownGeracoes['nGeracoes'];
							echo '<br />';
							echo '<a href="../Gestao_Tuna/Geracoes/ver_geracoes.php">Ver Gerações</a>';
						}
						else
						{
							echo '<br />';
							echo 'Não tem Gerações.';
							echo '</p>';
						}
		   			?>
		   		</div>	
		   		<div style="padding: 10px 0; width: 33%; float: left; text-align: center;">
		   			<?php
		   				 // Query que conta quantos Cargos existem na Tuna em sessão
	    				$sql = "SELECT COUNT(*) as nCargos FROM cargos WHERE idTuna = " . $_SESSION['idTuna'] . ";";	
						$resultnCargos = mysql_query($sql, $link) or die(mysql_error($link));		
						$rownCargos = mysql_fetch_array($resultnCargos);	
						if($rownCargos['nCargos'] > 0)
						{
							echo '<p>Cargos: ';
							echo $rownCargos['nCargos'];
							echo '<br />';
							echo '<a href="../Gestao_Tuna/cargos/ver_cargos.php">Ver Cargos</a>';
						}
						else
						{
							echo '<br />';
							echo 'Não tem Cargos.';
							echo '</p>';
						}
		   			?>
		   		</div>
		    		<?php
		    			$sql = 'SELECT * FROM membros_familia WHERE idTuna = '.$_SESSION['idTuna'].';';
						$resultAmigo = mysql_query($sql, $link) or die(mysql_error($link));
						if(mysql_num_rows($resultAmigo)>0) {
			    			echo '<p>Familia:</p><ul class="nav">';
							while($rowAmigo = mysql_fetch_array($resultAmigo)) {					
						
							    $sql = 'SELECT * from contas
							    		INNER JOIN tunas USING (idConta)
							    		WHERE idTuna = '.$rowAmigo['idTuna2'].';';
								$result = mysql_query($sql, $link) or die(mysql_error($link));
								$row= mysql_fetch_array($result);
							
								echo '<li><a href="perfil_tuna.php?idTuna='.$row['idTuna'].'"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../'.$row['img_path'].'" width="50" height="50"></span>'.$row['nome'].' - '.$rowAmigo['tipo'].'</a></li>';
							}
							echo '</ul>';
						}
		    		?>
		    </div>
	    </div>
	    <?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>