<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Perfil</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
	  		<ul class="nav">
				<li><a href="#">Perfil</a></li>
				<?php
					session_start();
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<?php
				// Query que selecciona toda a informação sobre a Conta e Utilizador que está em sessão
				$sql = "SELECT * FROM contas 
					INNER JOIN utilizadores USING (idConta)
					WHERE idConta = '" . $_SESSION['idConta'] . "';";
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
			?>
	    	<h2>Perfil | 
	    		<?php 
	    			echo $row['nome']; 
	    		?>
	    	</h2>
		    <div class="subcontent_top">
		    	<p><img src="../../<?php echo $row['img_path']; ?>" width="150" height="150"></p>
		    </div>
		    <div align="center" class="subcontent_top_center">
		    	<p>Nome: 
		    		<?php 
		    			echo $row['nome']; 
		    		?>
		    	</p>
		    	<p>Sexo: 
		    		<?php 
		    			switch($row['sexo'])
						{
							case 0:
								echo "Masculino";
								break;
							case 1:
								echo "Feminino";
								break;
						} 
		    		?>
		    	</p>
		    	<p>Data de Nascimento: 
		    		<?php 
		    			echo $row['data_nascimento']; 
		    		?>
		    	</p>
		    	<p>Email: 
		    		<?php 
		    			echo $row['email']; 
		    		?>
		    	</p>
		    	<p>Amigos: 
		    		<?php 
		    			// Query que conta quantos amigos o Utilizador em sessão tem
		    			$sql = 'SELECT COUNT(*) as nAmigos FROM amigos_utilizador 
    			   				INNER JOIN utilizadores USING (idUtilizador)
    			   				WHERE idConta = ' . $_SESSION['idConta'] . ' AND pedido_aceite = 1;';
						$resultAmigos = mysql_query($sql, $link) or die(mysql_error($link));
						$rowAmigos = mysql_fetch_array($resultAmigos);
						echo $rowAmigos['nAmigos'];
		    		?> 
		    		<a href="../Amigos/amigos_utilizador.php">Ver Amigos</a>
		    	</p>
		    </div>
		   	<div align="center" class="subcontent_top">
		   		<p><a href="../Conta/conta.php">Editar Perfil</a></p>
		   	</div>
		   	<div class="subcontent_bottom">
		    	<p>Sobre mim:</p>
		    	<p>
		    		<textarea disabled="true" rows="10" style="width: 98%;"><?php echo $row['sobre']; ?></textarea>
		    	</p>
		    	<?php
		    		// Query que selecciona as Tunas a que o Utilizador em sessão pertence
					$sql = "SELECT *, tunas.img_path AS imgTuna, contas.nome AS nomeConta FROM contas
							INNER JOIN tunas USING (idConta)
							INNER JOIN membros USING (idTuna) 
							WHERE idUtilizador = " . $_SESSION['idUtilizador'] . ";";
					$resultMembro = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($resultMembro) > 0)
					{
						echo '<p>Membro de:</p><ul class="nav" >';
						while($rowMembro = mysql_fetch_array($resultMembro))
						{
					   		echo '<li>';
					    	echo '<a href="perfil_membro_tuna.php?idMembro=' . $rowMembro['idMembro'] . '" style="vertical-align: center;">';
					    	echo '<span style="vertical-align: -17px; padding-right: 15px;">';
					    	echo '<img src="../../' . $rowMembro['imgTuna'] . '" width="50" height="50">';
					    	echo '</span>' . $rowMembro['nomeConta'] . ' | ' . $rowMembro['designacao'] . '</a>';
					    	echo '</li>';
						}
						echo '</ul>';
					}
				?>
	    		<?php
    				// Query que selecciona as Tunas que o Utilizador em sessão está a seguir
	    			$sql = "SELECT * FROM contas
	    					INNER JOIN tunas USING (idConta) 
	    					INNER JOIN seguidores USING (idTuna)
	    					WHERE idUtilizador = " . $_SESSION['idUtilizador'];
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo "<p>Tunas que sigo:</p>";
						echo "<ul class='nav'>";
						while($row = mysql_fetch_array($result))
						{
							echo "<li><a href='perfil_tuna.php?idTuna=" . $row['idTuna'] . "'><span style='vertical-align: -17px; padding-right: 15px;'><img src='../../".$row['img_path']."' width='50' height='50'></span> " . $row['nome'] . " | " . $row['designacao'] . "</a></li>";
						}
						echo "</ul>";
					}
	    		?>
			</div>
	    </div>
	    <?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>