<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Pedidos</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<h3>Pedidos</h3>
			<?php
				// Se a Conta em sessão não for do tipo Tuna
				if(!$_SESSION['tipoTuna'])
				{
					// Query que selecciona os pedidos de amizade feitos ao Utilizador em sessão
					$sql = "SELECT * FROM amigos_utilizador WHERE idUtilizador2 = " . $_SESSION['idUtilizador'] . " AND pedido_aceite = 0;";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					while($row = mysql_fetch_array($result))
					{
						// Query que selecciona toda a informação da Conta e do Utilizador da linha actual
						$sql = "SELECT * FROM contas 
								INNER JOIN utilizadores USING (idConta)
								WHERE idUtilizador = " . $row['idUtilizador'] . ";";
						$result2 = mysql_query($sql, $link) or die(mysql_error($link));
						$row2 = mysql_fetch_array($result2);
						echo $row2['nome'] . " | <a href='aceitar_pedido.php?idUtilizador=" . $row['idUtilizador'] . "'>Aceitar</a>";
						echo " | <a href='rejeitar_pedido.php?idUtilizador=" . $row['idUtilizador'] . "'>Rejeitar</a>";
						echo "<br />";
					}
				}
				// Se a Conta em sessão for do tipo Tuna
				else 
				{
					// Query que selecciona os pedidos de amizade feitos à Tuna em sessão
					$sql = "SELECT * FROM amigos_tuna WHERE idTuna2 = " . $_SESSION['idTuna'] . " AND amigos_tuna.pedido_aceite = 0;";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					while($row = mysql_fetch_array($result))
					{
						// Query que selecciona toda a informação da Conta e da Tuna da linha actual
						$sql = "SELECT * FROM contas 
								INNER JOIN tunas USING (idConta)
								WHERE idTuna = " . $row['idTuna'] . ";";
						$result2 = mysql_query($sql, $link) or die(mysql_error($link));
						$row2 = mysql_fetch_array($result2);
						echo $row2['nome'] . " | <a href='aceitar_pedido.php?idTuna=" . $row['idTuna'] . "'>Aceitar</a>";
						echo " | <a href='rejeitar_pedido.php?idTuna=" . $row['idTuna'] . "'>Rejeitar</a>";
						echo "<br />";
					}
				}
			?>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>