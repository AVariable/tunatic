<?php
	include ('../../ligacao_bd.php');
    session_start();
	
	// Se a sessão for do tipo Tuna
	if($_SESSION['tipoTuna']) 
	{
		$idTunaAmiga = $_GET['idTuna'];	//id da tuna amiga
		
		// Query que remove a relação de amizade entre a Tuna em sessão e a Tuna amiga
		$sql = 'DELETE FROM amigos_tuna WHERE idTuna = ' . $_SESSION['idTuna'] . ' AND idTuna2 = ' . $idTunaAmiga . ';';
		mysql_query($sql,$link) or die($link);
		
		// Query que remove a relação de amizade entre a Tuna amiga e a Tuna em sessão
		$sql = 'DELETE FROM amigos_tuna WHERE idTuna= ' . $idTunaAmiga . ' AND idTuna2 = ' . $_SESSION['idTuna'] . ';';
		mysql_query($sql,$link) or die($link);
		
		header("Location: amigos_tuna.php");
	} 
	// Se a sessão for do tipo Utilizador
	else 
	{
		$idAmigo = $_GET['idUtilizador'];	//id do amigo	
		
		// Query que remove a relação de amizade entre o Utilizador em sessão e o Utilizador amigo
		$sql = 'DELETE FROM amigos_utilizador WHERE idUtilizador = ' . $_SESSION['idUtilizador'] . ' AND idUtilizador2=' . $idAmigo . ';';
		mysql_query($sql,$link) or die($link); 
		
		// Query que remove a relação de amizade entre o Utilizador amigo e o Utilizador em sessão
		$sql = 'DELETE FROM amigos_utilizador WHERE idUtilizador = ' . $idAmigo . ' AND idUtilizador2 = ' . $_SESSION['idUtilizador'] . ';';
		mysql_query($sql,$link) or die($link);
		
		header("Location: amigos_utilizador.php");
	}
?>