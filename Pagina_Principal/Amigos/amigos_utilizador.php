<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Amigos</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<?php
				// Se o GET do id da Tuna existe
				if(isset($_GET['idTuna']))
				{
					// Query que selecciona toda a informação refente à Tuna e à Conta da Tuna
					$sql = "SELECT * FROM contas 
							INNER JOIN tunas USING (idConta)
							WHERE idTuna = " . $_GET['idTuna'];
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					$row = mysql_fetch_array($result);
					echo "<h3>Amigos de " . $row['nome'] . "</h3>";
				}
				// O GET do id da Tuna não existe
				else 
				{
					// Se o GET do id do Utilizador existe
					if(isset($_GET['idUtilizador']))
					{
						// Query que selecciona toda a informação refente ao Utilizador e à Conta do Utilizador
						$sql = "SELECT * FROM contas 
								INNER JOIN utilizadores USING (idConta)
								WHERE idUtilizador = " . $_GET['idUtilizador'];
						$result = mysql_query($sql, $link) or die(mysql_error($link));
						$row = mysql_fetch_array($result);
						echo "<h3>Amigos de " . $row['nome'] . "</h3>";
					}
					// O GET do id do Utilizador não existe
					else 
					{
						echo "<h3>Os meus amigos</h3>";
					}
				}
			?>
			<ul class="men">
			<?php
				// Se o GET do id do Utilizador existe
				if(isset($_GET['idUtilizador']))
				{
					// Query que selecciona todos os amigos do Utilizador que veio em GET
					$sql = "SELECT * FROM amigos_utilizador WHERE idUtilizador = " . $_GET['idUtilizador'];
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					while($row = mysql_fetch_array($result))
					{
						// Query que selecciona toda a informação da Conta e do Utilizador do amigo
						$sql = "SELECT * FROM contas
								INNER JOIN utilizadores USING (idConta)
								WHERE idUtilizador = " . $row['idUtilizador2'];
						$result2 = mysql_query($sql, $link) or die(mysql_error($link));
						$row2 = mysql_fetch_array($result2);
						// Se o pedido de esse amigo foi aceite referente ao Utilizador que veio em GET
						if($row['pedido_aceite'])
						{
							/* Se o id da Conta em sessão for diferente do id da Conta da linha actual cria um link para uma página de perfil
							   de Utilizador*/
							if($_SESSION['idConta'] != $row2['idConta'])
							{
								echo "<li><a href='../Perfil/perfil_utilizador.php?idUtilizador=" . $row2['idUtilizador'] . "'><span style='vertical-align: -17px; padding-right: 15px;'><img src='../../".$row2['img_path']."' width='50' height='50'></span>" . $row2['nome'] . "</a></li>";
							}
							/* Se o id da Conta em sessão for igual ao do id da Conta da linha actual cria um link para a própria página
							   de perfil da sessão*/
							else
							{
								echo "<li><a href='../Perfil/meu_perfil_utilizador.php?id=" . $row2['idConta'] . "'><span style='vertical-align: -17px; padding-right: 15px;'><img src='../../".$row2['img_path']."' width='50' height='50'></span>" . $row2['nome'] . "</a></li>";
							}
						}
					}
				}
				// Se o GET do id do Utilizador não existe
				else
				{
					// Query que selecciona todos os amigos do Utilizador em sessão
					$sql = "SELECT * FROM amigos_utilizador WHERE idUtilizador = " . $_SESSION['idUtilizador'];
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					while($row = mysql_fetch_array($result))
					{
						// Query que selecciona toda a informação da Conta e do Utilizador do amigo do Utilizador está em sessão
						$sql = "SELECT * FROM contas
								INNER JOIN utilizadores USING (idConta)
								WHERE idUtilizador = " . $row['idUtilizador2'];
						$result2 = mysql_query($sql, $link) or die(mysql_error($link));
						$row2 = mysql_fetch_array($result2);
						// Se o pedido de esse amigo foi aceite referente ao Utilizador em sessão cria um link para a página de perfil do Utilizador
						if($row['pedido_aceite'])
						{
							echo "<li><a href='../Perfil/perfil_utilizador.php?idUtilizador=" . $row2['idUtilizador'] . "'><span style='vertical-align: -17px; padding-right: 15px;'><img src='../../".$row2['img_path']."' width='50' height='50'></span>" . $row2['nome'] . "</a></li>";
						}
					}
				}
			?>
			</ul>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>