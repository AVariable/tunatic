<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Conta</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
	  		<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div align="center" class="content">
			<h1>Gestão de Conta</h1>
			<form action="processar_alteracao_conta.php" method="post" enctype="multipart/form-data">
			<?php
				// Query que selecciona toda a informação referente à Conta em sessão
				$sql = "SELECT * FROM contas WHERE idConta = '" . $_SESSION['idConta'] . "';";
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				
				echo'Email<br /> <input type="email" disabled="true" value="' . $row['email'] . '"/><br /><br />';
				echo'Password<br /> <input type="password" value="' . $row['password'] . '" name="password" /><br /><br />';
				echo'Nome<br /> <input type="text" value="' . $row['nome'] . '" name="nome" /><br /><br />';
				echo'Data de Nascimento<br /> <input type="date" value="' . $row['data_nascimento'] . '" name="dataNasc" /><br /><br />';
				echo'Sobre<br /> <textarea rows="5" name="sobre">' . $row['sobre'] . '</textarea><br /><br />';
				
				// Se a Conta em sessão for Tuna
				if($row['is_tuna'])
				{
					// Query que selecciona toda a informação referente à Tuna em sessão
					$sql = "SELECT * FROM tunas WHERE idConta = '" . $_SESSION['idConta'] . "';";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					$row = mysql_fetch_array($result);
					echo'Designação<br /> <input type="text" value="' . $row['designacao'] . '" name="designacao" /><br /><br />';
				}
				else
				{
					// Query que selecciona toda a informação referente ao utilizador em sessão
					$sql = "SELECT * FROM utilizadores WHERE idConta = '" . $_SESSION['idConta'] . "';";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					$row = mysql_fetch_array($result);
				}
			?>
			<img src="../../<?php echo $row['img_path']; ?>" width="200" height="200">
			<br />
			<input type="hidden" name="MAX_FILE_SIZE" value="1024000">
			<input type="file" name="origem" id="file">
			<br />
			<br />
			<input type="submit" value="Guardar Alterações" />
			</form>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>