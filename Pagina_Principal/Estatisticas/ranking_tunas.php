<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Feed</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
				?>
				
				<?php
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<h3>Ranking de Tunas</h3>
			<ul class="nav">
		 <?php
		 		$ranking = array();
		 		$sql = 'SELECT * FROM tunas;';
				$resTunas = mysql_query($sql, $link) or die(mysql_error($link));
				while($rTunas = mysql_fetch_array($resTunas)) {
					$nFestivais = 1;
					$somaPesos = 0;
					//seleciona e conta todos os festivais em que a tuna esteve presente	
					$sql = 'SELECT * FROM tunas_convidadas WHERE a_concurso = TRUE AND Tunas_idTuna = '.$rTunas['idTuna'].';';
					$resFest = mysql_query($sql, $link) or die(mysql_error($link));
					while($rFest = mysql_fetch_array($resFest)) {
						$nFestivais++;
						//seleciona e soma o peso de todos os premios
						$sql = 'SELECT * FROM premios WHERE idFestival = '.$rFest['Festivais_idFestival'].' AND vencedor = '.$rTunas['idTuna'].';';
						$resPrem = mysql_query($sql, $link) or die(mysql_error($link));
						while($rPrem = mysql_fetch_array($resPrem)) {
							$somaPesos = ($somaPesos + $rPrem['peso']);
							}
							
						}
					$alg = ($somaPesos / $nFestivais);
					$ranking[$rTunas['idTuna']]=$alg;
				}
				arsort($ranking);
				$num=0;
				foreach ($ranking as $key => $value) {
					$num++;
					$sql = 'SELECT * FROM tunas INNER JOIN contas USING (idConta) WHERE idTuna='.$key.';';
					$res = mysql_query($sql, $link) or die(mysql_error($link));
					$row = mysql_fetch_array($res);
					if($_SESSION['tipoTuna'])
					{
						if($_SESSION['idTuna'] == $row['idTuna'])
						{
							echo '<li><a href="../../Pagina_Principal/Perfil/meu_perfil_tuna.php">'.$num.'º <span style="vertical-align: -17px; padding-right: 15px; padding-left: 15px;"><img src="../../'.$row['img_path'].'" width="50" height="50"></span>'.$row['nome'].' | '.$row['designacao'].'</a></li>';
						}
						else
						{
							echo '<li><a href="../../Pagina_Principal/Perfil/perfil_tuna.php?idTuna='.$row['idTuna'].'">'.$num.'º <span style="vertical-align: -17px; padding-right: 15px; padding-left: 15px;"><img src="../../'.$row['img_path'].'" width="50" height="50"></span>'.$row['nome'].' | '.$row['designacao'].'</a></li>';
						}
					}
					else 
					{
						echo '<li><a href="../../Pagina_Principal/Perfil/perfil_tuna.php?idTuna='.$row['idTuna'].'">'.$num.'º <span style="vertical-align: -17px; padding-right: 15px; padding-left: 15px;"><img src="../../'.$row['img_path'].'" width="50" height="50"></span>'.$row['nome'].' | '.$row['designacao'].'</a></li>';
					}
				}
		 ?>  
		 </ul> 	
	    </div>
	  	<?php
	  		include("../../Pagina_Principal/sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>