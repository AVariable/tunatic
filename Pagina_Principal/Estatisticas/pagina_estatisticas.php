<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Feed</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			Tunatic
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
				?>
				
				<?php
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<h1 style="padding-left: 15px;">Estatisticas</h1>
			<!-- * Medias de Contas *-->
				<h3>Contas</h3><hr>
				<div style="width: 46%; float: left; text-align: right; padding-right: 2%;">
					<h5>% de Contas de Utilizador</h5>			
					<h5>% de Contas de Tunas</h5>
				</div>
				<div style="width: 46%; float: left; text-align: left; padding-left: 2%">
					<h5><?php echo get_percent_contas_utilizador();?>%</h5>
					<h5><?php echo get_percent_contas_tuna();?>%</h5>
				</div>
				<!-- * Medias de Tunas *-->
				<h3>Tunas</h3><hr>
				<div style="width: 46%; float: left; text-align: right; padding-right: 2%;">
					<h5>Media de Membros</h5>
					<h5>Media de Tunas Amigas</h5>
					<h5>Media de Seguidores</h5>			
					<h5>Media de Festivais</h5>
					<h5>Media de Premios</h5>
					<h5>% de Tunas Masculinas</h5>
					<h5>% de Tunas Femininas</h5>
					<h5>% de Tunas Mistas</h5>
				</div>
				<div style="width: 46%; float: left; text-align: left; padding-left: 2%">
					<h5><?php echo get_media_membros();//media de membros ?></h5>
					<h5><?php echo get_media_amigos_tuna();//media de tunas amigas ?></h5>
					<h5><?php echo get_media_seguidores();//media de seguidores ?></h5>
					<h5><?php echo get_media_festivais();//media de festivais ?></h5>
					<h5><?php echo get_media_premios();//media de premios ?></h5>
					<h5><?php echo get_percent_tunas_masculinas();//% de tunas masculinas?>%</h5>
					<h5><?php echo get_percent_tunas_femininas();//% de tunas femininas?>%</h5>
					<h5><?php echo get_percent_tunas_mistas();//% de tunas mistas?>%</h5>
				</div>
				<!-- * Medias de Tunas *-->
				<h3>Utilizadores</h3><hr>
				<div style="width: 46%; float: left; text-align: right; padding-right: 2%;">
					<h5>Media de Idade</h5>
					<h5>Media de Amigos</h5>
					<h5>% de Utilizadores de Sexo Masculino</h5>			
					<h5>% de Utilizadores de Sexo Feminino</h5>
					<h5>% de Utilizadores que pretencem a Tunas</h5>
					<h5>% de Utilizadores que não pretencem a Tunas</h5>
				</div>
				<div style="width: 46%; float: left; text-align: left; padding-left: 2%">
					<h5><?php echo get_media_idades();?></h5>
					<h5><?php echo get_media_amigos_utilizador(); ?></h5>
					<h5><?php echo get_percent_utilizadores_masculino();?>%</h5>
					<h5><?php echo get_percent_utilizadores_feminino();?>%</h5>
					<h5><?php echo get_percent_utilizadores_membros(); ?>%</h5>
					<h5><?php echo get_percent_utilizadores_nao_membros(); ?>%</h5>
				</div>
				<?php get_media_idades();?>
		</div>
	  	<?php
	  		include("../../Pagina_Principal/sidebar2.php");
	    	include("../../footer.php");
			
			//calcula percentagem de contas de utilizador
			function get_percent_contas_utilizador() {
				include('../../ligacao_bd.php');
				$sql = 'SELECT COUNT(*) AS n FROM contas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nTotal=$row['n'];
				$sql = 'SELECT COUNT(*) AS n FROM contas WHERE is_tuna = FALSE;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nMasc = $row['n'];
				if($nTotal != 0 && $nMasc != 0) {
					return round(($nMasc*100)/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula percentagem de contas de tuna
			function get_percent_contas_tuna() {
				include('../../ligacao_bd.php');
				$sql = 'SELECT COUNT(*) AS n FROM contas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nTotal=$row['n'];
				$sql = 'SELECT COUNT(*) AS n FROM contas WHERE is_tuna = TRUE;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nMasc = $row['n'];
				if($nTotal != 0 && $nMasc != 0) {
					return round(($nMasc*100)/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			
			//calcula media de membros por tuna;
			function get_media_Membros(){
				include('../../ligacao_bd.php');
				$nTunas=0;
				$somaMembros=0;
				$sql = 'SELECT idTuna FROM tunas;';
				$resTunas = mysql_query($sql, $link) or die(mysql_error($link));
				while ($rTunas = mysql_fetch_array($resTunas)) {
					$nTunas++;
					$sql = 'SELECT COUNT(idMembro) AS nMembros FROM membros WHERE idTuna='.$rTunas['idTuna'].';';
					$resMembros = mysql_query($sql, $link) or die(mysql_error($link));
					while ($rMembros = mysql_fetch_array($resMembros)) {
						$somaMembros = ($somaMembros+$rMembros['nMembros']);
					}
				}
				if($nTunas != 0 && $somaMembros != 0) {
					$media = ($somaMembros / $nTunas);
					return round($media, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}	
			//calcula media de amigos por tuna
			function get_media_amigos_tuna(){
				include('../../ligacao_bd.php');
				$nTunas=0;
				$somaAmigos=0;
				$sql = 'SELECT idTuna FROM tunas;';
				$resTunas = mysql_query($sql, $link) or die(mysql_error($link));
				while ($rTunas = mysql_fetch_array($resTunas)) {
					$nTunas++;
					$sql = 'SELECT COUNT(*) AS nAmigos FROM amigos_tuna WHERE idTuna='.$rTunas['idTuna'].';';
					$resAmigos = mysql_query($sql, $link) or die(mysql_error($link));
					while ($rAmigos = mysql_fetch_array($resAmigos)) {
						$somaAmigos = ($somaAmigos+$rAmigos['nAmigos']);
					}
				}
				if($nTunas != 0 && $somaAmigos != 0) {
					$media = ($somaAmigos / $nTunas);
					return round($media, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula media de seguidores por tuna
			function get_media_seguidores(){
				include('../../ligacao_bd.php');
				$n=0;
				$soma=0;
				$sql = 'SELECT idTuna FROM tunas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while ($row = mysql_fetch_array($result)) {
					$n++;
					$sql = 'SELECT COUNT(*) AS nSeguidores FROM seguidores WHERE idTuna='.$row['idTuna'].';';
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					while ($row2 = mysql_fetch_array($result2)) {
						$soma = ($soma+$row2['nSeguidores']);
					}
				}
				if($n != 0 && $soma != 0) {
					$media = ($soma / $n);
					return round($media, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}		
			//calcula media de festivais por tuna
			function get_media_festivais(){
				include('../../ligacao_bd.php');
				$n=0;
				$soma=0;
				$sql = 'SELECT idTuna FROM tunas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while ($row = mysql_fetch_array($result)) {
					$n++;
					$sql = 'SELECT COUNT(*) AS nFestivais FROM tunas_convidadas WHERE a_concurso=TRUE AND Tunas_idTuna='.$row['idTuna'].';';
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					while ($row2 = mysql_fetch_array($result2)) {
						$soma = ($soma+$row2['nFestivais']);
					}
				}
				if($n != 0 && $soma != 0) {
					$media = ($soma / $n);
					return round($media, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula media de premios por tuna
			function get_media_premios(){
				include('../../ligacao_bd.php');
				$n=0;
				$soma=0;
				$sql = 'SELECT idTuna FROM tunas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while ($row = mysql_fetch_array($result)) {
					$n++;
					$sql = 'SELECT COUNT(*) AS nPremios FROM premios WHERE vencedor='.$row['idTuna'].';';
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					while ($row2 = mysql_fetch_array($result2)) {
						$soma = ($soma+$row2['nPremios']);
					}
				}
				if($n != 0 && $soma != 0) {
					$media = ($soma / $n);
					return round($media, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula % de tunas masculinas
			function get_percent_tunas_masculinas(){
				include('../../ligacao_bd.php');
				$sql = 'SELECT COUNT(*) AS n FROM tunas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nTotal=$row['n'];
				$sql = 'SELECT COUNT(*) AS n FROM tunas WHERE tipo = 0;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nMasc = $row['n'];
				if($nTotal != 0 && $nMasc != 0) {
					return round(($nMasc*100)/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula % de tunas femininas
			function get_percent_tunas_femininas(){
				include('../../ligacao_bd.php');
				$sql = 'SELECT COUNT(*) AS n FROM tunas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nTotal=$row['n'];
				$sql = 'SELECT COUNT(*) AS n FROM tunas WHERE tipo = 1;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nMasc = $row['n'];
				if($nTotal != 0 && $nMasc != 0) {
					return round(($nMasc*100)/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula % de tunas mistas
			function get_percent_tunas_mistas(){
				include('../../ligacao_bd.php');
				$sql = 'SELECT COUNT(*) AS n FROM tunas;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nTotal=$row['n'];
				$sql = 'SELECT COUNT(*) AS n FROM tunas WHERE tipo = 2;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nMasc = $row['n'];
				if($nTotal != 0 && $nMasc != 0) {
					return round(($nMasc*100)/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula % de utilizadores do sexo masculino
			function get_percent_utilizadores_masculino(){
				include('../../ligacao_bd.php');
				$sql = 'SELECT COUNT(*) AS n FROM utilizadores;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nTotal=$row['n'];
				$sql = 'SELECT COUNT(*) AS n FROM utilizadores WHERE sexo = FALSE;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nMasc = $row['n'];
				if($nTotal != 0 && $nMasc != 0) {
					return round(($nMasc*100)/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula % de utilizadores do sexo feminino
			function get_percent_utilizadores_feminino(){
				include('../../ligacao_bd.php');
				$sql = 'SELECT COUNT(*) AS n FROM utilizadores;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nTotal=$row['n'];
				$sql = 'SELECT COUNT(*) AS n FROM utilizadores WHERE sexo = TRUE;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				$nMasc = $row['n'];
				if($nTotal != 0 && $nMasc != 0) {
					return round(($nMasc*100)/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula % de utilizadores que pretencem a tunas
			function get_percent_utilizadores_membros(){
				include('../../ligacao_bd.php');
				$nU=0;
				$nM=0;
				$sql = 'SELECT idUtilizador FROM utilizadores;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while ($row = mysql_fetch_array($result)) {
					$nU++;
					$sql = 'SELECT idUtilizador FROM membros WHERE idUtilizador='.$row['idUtilizador'].';';
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result2)>0){
						$nM++;
					}
				}
				if($nU != 0 && $nM != 0) {
					return round(($nM*100)/$nU, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			//calcula media de idades dos utilizadores
			function get_media_idades(){
				$nTotal=0;
				$soma=0;
				include('../../ligacao_bd.php');
				$sql = 'SELECT floor(datediff (now(), data_nascimento)/365) AS idade FROM contas WHERE is_tuna=FALSE AND data_nascimento!=0;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while ($row = mysql_fetch_array($result)) {
					$nTotal++;
					$soma=($soma+$row['idade']);
				}
				if($nTotal!=0){
					return round($soma/$nTotal, 0, PHP_ROUND_HALF_DOWN);
				}
			}
			//calcula media de amigos por utilizador
			function get_media_amigos_utilizador(){
				include('../../ligacao_bd.php');
				$nU=0;
				$somaAmigos=0;
				$sql = 'SELECT idUtilizador FROM utilizadores;';
				$resU = mysql_query($sql, $link) or die(mysql_error($link));
				while ($rU = mysql_fetch_array($resU)) {
					$nU++;
					$sql = 'SELECT COUNT(*) AS nAmigos FROM amigos_utilizador WHERE idUtilizador='.$rU['idUtilizador'].';';
					$resAmigos = mysql_query($sql, $link) or die(mysql_error($link));
					while ($rAmigos = mysql_fetch_array($resAmigos)) {
						$somaAmigos = ($somaAmigos+$rAmigos['nAmigos']);
					}
				}
				if($nU != 0 && $somaAmigos != 0) {
					$media = ($somaAmigos / $nU);
					return round($media, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			function get_percent_utilizadores_nao_membros(){
				include('../../ligacao_bd.php');
				$nU=0;
				$nM=0;
				$sql = 'SELECT idUtilizador FROM utilizadores;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while ($row = mysql_fetch_array($result)) {
					$nU++;
					$sql = 'SELECT idUtilizador FROM membros WHERE idUtilizador='.$row['idUtilizador'].';';
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result2)==0){
						$nM++;
					}
				}
				if($nU != 0 && $nM != 0) {
					return round(($nM*100)/$nU, 0, PHP_ROUND_HALF_DOWN);
				} else {
					return 0;
				}
			}
			
			
		
	    ?>
    </div>
</body>
</html>