<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Feed</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			Tunatic
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
				?>
				<?php
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo'<li><a href="../Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<h3>Feed de Notícias</h3>
		    <form action="processar_criar_post.php" method="post">
		    	<p>
		    		<textarea style="width: 98%;" rows="3" name="texto">Crie o seu post aqui.</textarea>
		    		<input style="float: right; margin-right: 4px;" type="submit" value="Publicar" />
		    	</p>
		    </form>
		    <br />
		    <?php
		    	if($_SESSION['tipoTuna'])
				{
					$sql = 'SELECT * FROM
							(
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							WHERE idConta = ' . $_SESSION['idConta'] . '
							UNION
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							INNER JOIN tunas USING (idConta)
							INNER JOIN amigos_tuna ON tunas.idTuna = amigos_tuna.idTuna2
							WHERE amigos_tuna.idTuna = ' . $_SESSION['idTuna'] . '
							UNION
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							INNER JOIN utilizadores USING (idConta)
							INNER JOIN seguidores USING (idUtilizador)
							WHERE seguidores.idTuna = ' . $_SESSION['idTuna'] . '
							UNION
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							INNER JOIN utilizadores USING (idConta)
							INNER JOIN membros USING (idUtilizador)
							WHERE membros.idTuna = ' . $_SESSION['idTuna'] . '
							) as Posts
							ORDER BY timestamp DESC;';
				}
				else
				{
			    	$sql = 'SELECT * FROM
							(
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							WHERE idConta = ' . $_SESSION['idConta'] . '
							UNION
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							INNER JOIN utilizadores USING (idConta)
							INNER JOIN amigos_utilizador ON utilizadores.idUtilizador = amigos_utilizador.idUtilizador2
							WHERE amigos_utilizador.idUtilizador = ' . $_SESSION['idUtilizador'] . '
							UNION
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							INNER JOIN tunas USING (idConta)
							INNER JOIN seguidores USING (idTuna)
							WHERE seguidores.idUtilizador = ' . $_SESSION['idUtilizador'] . '
							UNION
							SELECT contas.idConta, contas.nome, posts.idPost, posts.timestamp, posts.testo FROM posts
							INNER JOIN contas USING (idConta)
							INNER JOIN tunas USING (idConta)
							INNER JOIN membros USING (idTuna)
							WHERE membros.idUtilizador = ' . $_SESSION['idUtilizador'] . '
							) as Posts
							ORDER BY timestamp DESC;';
				}
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				while($row = mysql_fetch_array($result))
				{
					$sql = 'SELECT * FROM tunas WHERE idConta = ' . $row['idConta'] . ';';
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result2) == 0)
					{
						$sql = 'SELECT * FROM utilizadores WHERE idConta = ' . $row['idConta'] . ';';
						$result2 = mysql_query($sql, $link) or die(mysql_error($link));
						$row2 = mysql_fetch_array($result2);
						$imgPath = $row2['img_path'];
					}
					else 
					{
						$row2 = mysql_fetch_array($result2);
						$imgPath = $row2['img_path'];
					}
					echo '<hr>';
					if(!$_SESSION['tipoTuna'])
					{
						if(isset($row2['idUtilizador']))
						{
							if($_SESSION['idUtilizador'] == $row2['idUtilizador'])
							{
								echo '<p style="margin-left: 15px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/meu_perfil_utilizador.php"><img src="../../' . $imgPath .'" width="50" height="50"><span style="padding-left: 15px; vertical-align: 18px;">' . $row['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 18px;"> | ' . $row['timestamp'] . '</span>';
							}
							else 
							{
								echo '<p style="margin-left: 15px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_utilizador.php?idUtilizador=' . $row2['idUtilizador'] . '"><img src="../../' . $imgPath .'" width="50" height="50"><span style="padding-left: 15px; vertical-align: 18px;">' . $row['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 18px;"> | ' . $row['timestamp'] . '</span>';	
							}
						}
						else
						{
							echo '<p style="margin-left: 15px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_tuna.php?idTuna=' . $row2['idTuna'] . '"><img src="../../' . $imgPath .'" width="50" height="50"><span style="padding-left: 15px; vertical-align: 18px;">' . $row['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 18px;"> | ' . $row['timestamp'] . '</span>';	
						}
					}
					else
					{
						if(isset($row2['idTuna']))
						{
							if($_SESSION['idTuna'] == $row2['idTuna'])
							{
								echo '<p style="margin-left: 15px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/meu_perfil_tuna.php"><img src="../../' . $imgPath .'" width="50" height="50"><span style="padding-left: 15px; vertical-align: 18px;">' . $row['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 18px;"> | ' . $row['timestamp'] . '</span>';
							}
							else 
							{
								echo '<p style="margin-left: 15px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_tuna.php?idTuna=' . $row2['idTuna'] . '"><img src="../../' . $imgPath .'" width="50" height="50"><span style="padding-left: 15px; vertical-align: 18px;">' . $row['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 18px;"> | ' . $row['timestamp'] . '</span>';	
							}
						}
						else
						{
							echo '<p style="margin-left: 15px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_utilizador.php?idUtilizador=' . $row2['idUtilizador'] . '"><img src="../../' . $imgPath .'" width="50" height="50"><span style="padding-left: 15px; vertical-align: 18px;">' . $row['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 18px;"> | ' . $row['timestamp'] . '</span>';	
						}
					}
					echo '<br />' . $row['testo'];
					$sql = 'SELECT * FROM gostos_posts WHERE idPost = ' . $row['idPost'] . ' AND idConta = ' . $_SESSION['idConta'] . ';';
					$checkgostoPost = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($checkgostoPost) == 0)
					{
						echo '<br /><a href="processar_like_post.php?idPost=' . $row['idPost'] . '">Gosto</a>';
					}
					else
					{
						echo '<br /><a href="processar_dislike_post.php?idPost=' . $row['idPost'] . '">Não Gosto</a>';
					}
					$sql = 'SELECT COUNT(*) as nGostos FROM gostos_posts WHERE idPost = ' . $row['idPost'] . ';';
					$nGostos = mysql_query($sql, $link) or die(mysql_error($link));
					$rownGostos = mysql_fetch_array($nGostos);
					echo ' ' . $rownGostos['nGostos'] . ' Gostam disto. ';
					if($_SESSION['idConta'] == $row2['idConta'])
					{
						echo '<a href="processar_remover_post.php?idPost=' . $row['idPost'] . '"><span style="color: red;">Apagar Post</span></a>';
					}
					echo '</p>';
					$sql = 'SELECT * FROM comentarios 
							INNER JOIN contas USING (idConta)
							WHERE idPost = ' . $row['idPost'] . ';';
					$result2 = mysql_query($sql, $link) or die(mysql_error($link));
					while($row2 = mysql_fetch_array($result2))
					{
						$sql = 'SELECT * FROM tunas WHERE idConta = ' . $row2['idConta'] . ';';
						$result3 = mysql_query($sql, $link) or die(mysql_error($link));
						if(mysql_num_rows($result3) == 0)
						{
							$sql = 'SELECT * FROM utilizadores WHERE idConta = ' . $row2['idConta'] . ';';
							$result3 = mysql_query($sql, $link) or die(mysql_error($link));
							$row3 = mysql_fetch_array($result3);
							$imgPath = $row3['img_path'];
						}
						else 
						{
							$row3 = mysql_fetch_array($result3);
							$imgPath = $row3['img_path'];
						}
						if(!$_SESSION['tipoTuna'])
						{
							if(isset($row3['idUtilizador']))
							{
								if($_SESSION['idUtilizador'] == $row3['idUtilizador'])
								{
									echo '<p style="margin-left: 50px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/meu_perfil_utilizador.php"><img src="../../' . $imgPath .'" width="25" height="25"><span style="padding-left: 15px; vertical-align: 6px;">' . $row2['nome'] . '</span><span style="padding-left: 15px; vertical-align: 6px;"> | ' . $row2['timestamp'] . '</span>';
								}
								else 
								{
									echo '<p style="margin-left: 50px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_utilizador.php?idUtilizador=' . $row3['idUtilizador'] . '"><img src="../../' . $imgPath .'" width="25" height="25"><span style="padding-left: 15px; vertical-align: 6px;">' . $row2['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 6px;"> | ' . $row2['timestamp'] . '</span>';
								}
							}
							else
							{
								echo '<p style="margin-left: 50px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_tuna.php?idTuna=' . $row3['idTuna'] . '"><img src="../../' . $imgPath .'" width="25" height="25"><span style="padding-left: 15px; vertical-align: 6px;">' . $row2['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 6px;"> | ' . $row2['timestamp'] . '</span>';
							}
						}
						else
						{
							if(isset($row3['idTuna']))
							{
								if($_SESSION['idTuna'] == $row3['idTuna'])
								{
									echo '<p style="margin-left: 50px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/meu_perfil_tuna.php"><img src="../../' . $imgPath .'" width="25" height="25"><span style="padding-left: 15px; vertical-align: 6px;">' . $row2['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 6px;"> | ' . $row2['timestamp'] . '</span>';
								}
								else 
								{
									echo '<p style="margin-left: 50px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_tuna.php?idTuna=' . $row3['idTuna'] . '"><img src="../../' . $imgPath .'" width="25" height="25"><span style="padding-left: 15px; vertical-align: 6px;">' . $row2['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 6px;"> | ' . $row2['timestamp'] . '</span>';
								}
							}
							else
							{
								echo '<p style="margin-left: 50px; margin-right: 15px; background-color: #FFFFFF;"><a href="../Perfil/perfil_utilizador.php?idUtilizador=' . $row3['idUtilizador'] . '"><img src="../../' . $imgPath .'" width="25" height="25"><span style="padding-left: 15px; vertical-align: 6px;">' . $row2['nome'] . '</span></a><span style="padding-left: 15px; vertical-align: 6px;"> | ' . $row2['timestamp'] . '</span>';
							}
						}
						echo '<br />';
						echo $row2['texto'];
						$sql = 'SELECT * FROM gostos_comentarios WHERE idComentario = ' . $row2['idComentario'] . ' AND idConta = ' . $_SESSION['idConta'] . ';';
						$checkgostoComentario = mysql_query($sql, $link) or die(mysql_error($link));
						if(mysql_num_rows($checkgostoComentario) == 0)
						{
							echo '<br /><a href="processar_like_comentario.php?idComentario=' . $row2['idComentario'] . '">Gosto</a>';
						}
						else
						{
							echo '<br /><a href="processar_dislike_comentario.php?idComentario=' . $row2['idComentario'] . '">Não Gosto</a>';
						}
						$sql = 'SELECT COUNT(*) as nGostos FROM gostos_comentarios WHERE idComentario = ' . $row2['idComentario'] . ';';
						$nGostos = mysql_query($sql, $link) or die(mysql_error($link));
						$rownGostos = mysql_fetch_array($nGostos);
						echo ' ' . $rownGostos['nGostos'] . ' Gostam disto. ';
						if($_SESSION['idConta'] == $row2['idConta'])
						{
							echo '<a href="processar_remover_comentario.php?idComentario=' . $row2['idComentario'] . '"><span style="color: red;">Apagar Comentario</span></a>';
						}
						echo '</p>';
					}
					echo '</p>';
					echo '<form action="processar_criar_comentario.php?idPost=' . $row['idPost'] . '" method="post">';
					echo '<p style="padding-left: 50px;"><textarea style="width: 70%;" rows="1" name="texto">Crie o seu comentário aqui.</textarea>
						  <span style="vertical-align: 8px;"><input type="submit" value="Comentar"></span></p>';
					echo '</form>';
				}
		    ?>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>