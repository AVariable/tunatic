<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Festival</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
				?>
				<li><a href="festivais.php">Festivais</a></li>
				<?php
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<?php
				$sql = 'SELECT * FROM festivais WHERE idFestival = ' . $_GET['idFestival'] . ';';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				if($_SESSION['tipoTuna'])
				{
					if($_SESSION['idTuna'] == $row['tuna_organizadora'])
					{
						echo '<h3>' . $row['nome'] . '| <a href="editar_festival.php?idFestival=' . $_GET['idFestival'] . '">Editar</a></h3>';
					}
					else
					{
						echo '<h3>' . $row['nome'] . '</h3>';
					}
				}
				else
				{
					echo '<h3>' . $row['nome'] . '</h3>';
				}
			?>
			<p>Local: <?php echo $row['local']; ?></p>
			<p>Sobre:<br /><textarea disabled="true" style="width: 98%;" rows="3"><?php echo $row['sobre']; ?></textarea></p>
			<p>Data de ínicio: <?php echo $row['data_inicio']; ?></p>
			<p>Data de fim: <?php echo $row['data_fim']; ?></p>
			<h3>Tuna Organizadora</h3>
			<?php
				$sql = 'SELECT * FROM contas
						INNER JOIN tunas USING (idConta) 
						WHERE idTuna = ' . $row['tuna_organizadora'] . ';';
				$resultTuna = mysql_query($sql, $link) or die(mysql_error($link));
				$rowTuna = mysql_fetch_array($resultTuna);
			?>
			<ul class="men">
				<?php
					if($_SESSION['tipoTuna'])
					{
						if($_SESSION['idTuna'] == $rowTuna['idTuna'])
						{
							echo '<li><a href="../Perfil/meu_perfil_tuna.php"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowTuna['img_path'] . '" width="50" height="50"></span>' . $rowTuna['nome'] . ' | ' . $rowTuna['designacao'] . '</a></li>';
						}
						else
						{
							echo '<li><a href="../Perfil/perfil_tuna.php?idTuna=' . $rowTuna['idTuna'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowTuna['img_path'] . '" width="50" height="50"></span>' . $rowTuna['nome'] . ' | ' . $rowTuna['designacao'] . '</a></li>';
						}
					}
					else 
					{
						echo '<li><a href="../Perfil/perfil_tuna.php?idTuna=' . $rowTuna['idTuna'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowTuna['img_path'] . '" width="50" height="50"></span>' . $rowTuna['nome'] . ' | ' . $rowTuna['designacao'] . '</a></li>';
					} 
				?>
			</ul>
			<?php
				$sql = 'SELECT * FROM contas
						INNER JOIN tunas USING (idConta)
						INNER JOIN tunas_Convidadas ON tunas_convidadas.Tunas_idTuna = tunas.idTuna 
						WHERE Festivais_idFestival = ' . $_GET['idFestival'] . ' AND a_concurso = 1;';
				$resultConvidadas = mysql_query($sql, $link) or die(mysql_error($link));
				if(mysql_num_rows($resultConvidadas) > 0)
				{
					echo '<h3>Tunas Convidadas a Concurso</h3>';
					echo '<ul class="men">';
					while($rowConvidada = mysql_fetch_array($resultConvidadas))
					{
						if($_SESSION['tipoTuna'])
						{
							if($rowConvidada['idTuna'] == $_SESSION['idTuna'])
							{
								echo '<li><a href="../Perfil/meu_perfil_tuna.php"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowConvidada['img_path'] . '" width="50" height="50"></span>' . $rowConvidada['nome'] . ' | ' . $rowConvidada['designacao'] . '</a></li>';
							}
							else
							{
								echo '<li><a href="../Perfil/perfil_tuna.php?idTuna=' . $rowConvidada['idTuna'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowConvidada['img_path'] . '" width="50" height="50"></span>' . $rowConvidada['nome'] . ' | ' . $rowConvidada['designacao'] . '</a></li>';
							}
						}
						else
						{
							echo '<li><a href="../Perfil/perfil_tuna.php?idTuna=' . $rowConvidada['idTuna'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowConvidada['img_path'] . '" width="50" height="50"></span>' . $rowConvidada['nome'] . ' | ' . $rowConvidada['designacao'] . '</a></li>';
						}
					}
					echo '</ul>';
				}

				$sql = 'SELECT * FROM contas
						INNER JOIN tunas USING (idConta)
						INNER JOIN tunas_Convidadas ON tunas_convidadas.Tunas_idTuna = tunas.idTuna 
						WHERE Festivais_idFestival = ' . $_GET['idFestival'] . ' AND a_concurso = 0	;';
				$resultConvidadas = mysql_query($sql, $link) or die(mysql_error($link));
				if(mysql_num_rows($resultConvidadas) > 0)
				{
					echo '<h3>Tunas Convidadas extra Concurso</h3>';
					echo '<ul class="men">';
					while($rowConvidada = mysql_fetch_array($resultConvidadas))
					{
						if($_SESSION['tipoTuna'])
						{
							if($rowConvidada['idTuna'] == $_SESSION['idTuna'])
							{
								echo '<li><a href="../Perfil/meu_perfil_tuna.php"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowConvidada['img_path'] . '" width="50" height="50"></span>' . $rowConvidada['nome'] . ' | ' . $rowConvidada['designacao'] . '</a></li>';
							}
							else
							{
								echo '<li><a href="../Perfil/perfil_tuna.php?idTuna=' . $rowConvidada['idTuna'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowConvidada['img_path'] . '" width="50" height="50"></span>' . $rowConvidada['nome'] . ' | ' . $rowConvidada['designacao'] . '</a></li>';
							}
						}
						else
						{
							echo '<li><a href="../Perfil/perfil_tuna.php?idTuna=' . $rowConvidada['idTuna'] . '"><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $rowConvidada['img_path'] . '" width="50" height="50"></span>' . $rowConvidada['nome'] . ' | ' . $rowConvidada['designacao'] . '</a></li>';
						}
					}
					echo '</ul>';
				}
				
				$sql = 'SELECT * FROM premios
						INNER JOIN festivais USING (idFestival)
						WHERE idFestival = ' . $_GET['idFestival'] . ';';
				$resultPremios = mysql_query($sql, $link) or die(mysql_error($link));
				if(mysql_num_rows($resultPremios) > 0)
				{
					echo '<h3>Prémios</h3>';
					echo '<ul class="men">';
					while($rowPremio = mysql_fetch_array($resultPremios))
					{
						echo '<li style="background-color: #10626D; color: white;">Prémio: ' . $rowPremio['designacao'];
						if($rowPremio['vencedor'] != NULL)
						{
							$sql = 'SELECT contas.nome, tunas.designacao FROM contas
									INNER JOIN tunas USING (idConta)
									INNER JOIN premios ON premios.vencedor = tunas.idTuna
									WHERE idPremio = ' . $rowPremio['idPremio'];
							$resultTuna = mysql_query($sql, $link) or die(mysql_error($link));
							$rowTuna = mysql_fetch_array($resultTuna);
							echo '<br />Tuna vencedora: ' . $rowTuna['nome'] . ' | ' . $rowTuna['designacao'] . '</li><br/>';
						}
						else
						{
							echo '<br /><br />Prémio não atribuido</li>';
						}
					}
					echo '</ul>';
				}
			?>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>