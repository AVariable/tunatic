<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Festival</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
				?>
				<li><a href="festivais.php">Festivais</a></li>
				<?php
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<?php
				$sql = 'SELECT * FROM festivais WHERE idFestival = ' . $_GET['idFestival'] . ';';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
			?>
			<form action="processar_editar_festival.php" method="post" >
				<input type="hidden" name="idFestival" value="<?php echo $row['idFestival'] ?>" />
				<p>Nome<br/><input type="text" name="nome" value="<?php echo $row['nome']; ?>" /></p>
				<p>Local<br/><input type="text" name="local" value="<?php echo $row['local']; ?>" /></p>
				<p>Sobre<br/><textarea style="width: 95%;" rows="5" name="sobre"><?php echo $row['sobre']; ?></textarea></p>
				<p>Data de Ínicio<br/><input type="datetime-local" name="dataInicio" value="<?php echo str_replace(" ", "T", $row['data_inicio']); ?>" /></p>
				<p>Data de Fim<br/><input type="datetime-local" name="dataFim" value="<?php echo str_replace(" ", "T", $row['data_fim']); ?>" /></p>
				<p><input type="submit" value="Alterar" /></p>
			</form>
			<p><a href="pesquisar_tuna.php?idFestival=<?php echo $_GET['idFestival']; ?>">Convidar Tunas</a></p>
			<?php
				// Query que selecciona todos as tunas convidadas a concurso
				$sql = 'SELECT * FROM contas
						INNER JOIN tunas USING (idConta)
						INNER JOIN tunas_convidadas ON tunas.idTuna = tunas_convidadas.Tunas_idTuna
						WHERE a_concurso = 1 AND Festivais_idFestival = ' . $_GET['idFestival'] . ';';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				if(mysql_num_rows($result) > 0)
				{
					echo '<h3>Tunas a Concurso</h3>';
					echo '<ul class="men">';
					while($row = mysql_fetch_array($result))
					{
						echo '<li><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $row['img_path'] . '" width="50" height="50"></span>' . $row['nome'] . '
						<a href="remover_tuna_convidada.php?idTuna=' . $row['idTuna'] .'&idFestival=' . $_GET['idFestival'] . '">Remover</a></li>';
					}
					echo '</ul>';
				}
				// Query que selecciona todos as tunas convidadas a concurso
				$sql = 'SELECT * FROM contas
						INNER JOIN tunas USING (idConta)
						INNER JOIN tunas_convidadas ON tunas.idTuna = tunas_convidadas.Tunas_idTuna
						WHERE a_concurso = 0 AND Festivais_idFestival = ' . $_GET['idFestival'] . ';';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				if(mysql_num_rows($result) > 0)
				{
					echo '<h3>Tunas extra Concurso</h3>';
					echo '<ul class="men">';
					while($row = mysql_fetch_array($result))
					{
						echo '<li><span style="vertical-align: -17px; padding-right: 15px;"><img src="../../' . $row['img_path'] . '" width="50" height="50"></span>' . $row['nome'] . '
						<a href="remover_tuna_convidada.php?idTuna=' . $row['idTuna'] .'&idFestival=' . $_GET['idFestival'] . '">Remover</a></li>';
					}
					echo '</ul>';
				}
			?>
			<p><a href="adicionar_premio.php?idFestival=<?php echo $_GET['idFestival']; ?>">Adicionar Prémios</a></p>
			<?php
				$sql = 'SELECT * FROM premios
						INNER JOIN festivais USING (idFestival)
						WHERE idFestival = ' . $_GET['idFestival'] . ';';
				$resultPremios = mysql_query($sql, $link) or die(mysql_error($link));
				if(mysql_num_rows($resultPremios) > 0)
				{
					echo '<h3>Prémios</h3>';
					echo '<ul class="men">';
					while($rowPremio = mysql_fetch_array($resultPremios))
					{
						echo '<li style="background-color: #10626D; color: white;">Prémio: ' . $rowPremio['designacao'];
						if($rowPremio['vencedor'] != NULL)
						{
							$sql = 'SELECT contas.nome, tunas.designacao FROM contas
									INNER JOIN tunas USING (idConta)
									INNER JOIN premios ON premios.vencedor = tunas.idTuna
									WHERE idPremio = ' . $rowPremio['idPremio'];
							$resultTuna = mysql_query($sql, $link) or die(mysql_error($link));
							$rowTuna = mysql_fetch_array($resultTuna);
							echo '<br />Tuna vencedora: ' . $rowTuna['nome'] . ' | ' . $rowTuna['designacao'] . '<a href="remover_atribuir_premio.php?idPremio=' . $rowPremio['idPremio'] . '&idFestival=' . $_GET['idFestival'] . '">Remover Atribuição</a></li>';
						}
						else
						{
							echo '<br /><br />Prémio não atribuido <a href="atribuir_premio.php?idPremio=' . $rowPremio['idPremio'] . '">Atribuir Prémio</a></li>';
						}
						echo '<li><a href="remover_premio.php?idPremio=' . $rowPremio['idPremio'] . '&idFestival=' . $_GET['idFestival'] . '">Remover Prémio</a></li><br />';
					}
					echo '</ul>';
				}
			?>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>