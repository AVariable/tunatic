<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Eventos</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
				?>
				<li><a href="festivais.php">Festivais</a></li>
				<?php
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<h3>Festivais</h3>
			<p><a href="criar_festival.php">Criar Festival</a></p>
			<?php
				// Se a conta em sessão for do tipo Tuna
				if($_SESSION['tipoTuna'])
				{
					// Query que selecciona todos os festivais que a Tuna em sessão criou
					$sql = 'SELECT * FROM festivais WHERE tuna_organizadora = ' . $_SESSION['idTuna'] . ';';
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo '<p>Os meus Festivais</p>';
						echo '<ul class="men">';
						while($row = mysql_fetch_array($result))
						{
							echo '<li><a href="ver_festival.php?idFestival=' . $row['idFestival'] . '">' . $row['nome'] . '</a></li>';
						}
						echo '</ul>';
					}
				}
				// Se a conta em sessão for do tipo Tuna
				if($_SESSION['tipoTuna'])
				{
					// Query que selecciona todos os festivais que a Tuna em sessão participou
					$sql = 'SELECT * FROM festivais 
							INNER JOIN tunas_convidadas ON tunas_convidadas.Festivais_idFestival = festivais.idFestival
							WHERE Tunas_idTuna = ' . $_SESSION['idTuna'] . ' AND a_concurso = 0;';
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo '<p>Festivais extra Concurso</p>';
						echo '<ul class="men">';
						while($row = mysql_fetch_array($result))
						{
							echo '<li><a href="ver_festival.php?idFestival=' . $row['idFestival'] . '">' . $row['nome'] . '</a></li>';
						}
						echo '</ul>';
					}
				}
				// Se a conta em sessão for do tipo Tuna
				if($_SESSION['tipoTuna'])
				{
					// Query que selecciona todos os festivais que a Tuna em sessão participou
					$sql = 'SELECT * FROM festivais 
							INNER JOIN tunas_convidadas ON tunas_convidadas.Festivais_idFestival = festivais.idFestival
							WHERE Tunas_idTuna = ' . $_SESSION['idTuna'] . ' AND a_concurso = 1;';
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo '<p>Festivais a Concurso</p>';
						echo '<ul class="men">';
						while($row = mysql_fetch_array($result))
						{
							echo '<li><a href="ver_festival.php?idFestival=' . $row['idFestival'] . '">' . $row['nome'] . '</a></li>';
						}
						echo '</ul>';
					}
				}
			?>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>