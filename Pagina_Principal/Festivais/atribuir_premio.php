<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Festival</title>
	<link rel="stylesheet" type="text/css" href="../../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="../Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='../Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='../Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
						echo "<li><a href='../Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
				?>
				<li><a href="festivais.php">Festivais</a></li>
				<?php
					include('../../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo"<li><a href='../Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='../Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<form action="processar_atribuir_premio.php" method="post">
				<input type="hidden" name="idFestival" value="<?php 
					$sql = 'SELECT * FROM premios WHERE idPremio = ' . $_GET['idPremio'] . ';';
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					$row = mysql_fetch_array($result);
					echo $row['idFestival'];
				?>"/>
				<input type="hidden" name="idPremio" value="<?php echo $_GET['idPremio']; ?>" />
				<p>Atribuir prémio a:</p>
				<p><select name="idTuna">
					<?php
						$sql = 'SELECT tunas.idTuna, contas.nome, tunas.designacao FROM contas
								INNER JOIN tunas USING (idConta)
								INNER JOIN tunas_convidadas ON tunas_convidadas.Tunas_idTuna = tunas.idTuna
								INNER JOIN festivais ON tunas_convidadas.Festivais_idFestival = festivais.idFestival
								INNER JOIN premios USING (idFestival)
								WHERE idPremio = ' . $_GET['idPremio'] . ' AND tunas_convidadas.a_concurso = 1;';
						$result = mysql_query($sql, $link) or die(mysql_error($link));
						while($row = mysql_fetch_array($result))
						{
							echo '<option value="' . $row['idTuna'] . '">' . $row['nome'] . ' | ' . $row['designacao'] . '</option>';
						}
					?>	
				</select></p>
				<p><input type="submit" value="Atribuir" /></p>
			</form>
	    </div>
	  	<?php
	  		include("../sidebar2.php");
	    	include("../../footer.php");
	    ?>
    </div>
</body>
</html>