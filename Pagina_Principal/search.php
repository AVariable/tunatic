<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Tunatic - Search</title>
	<link rel="stylesheet" type="text/css" href="../CSS/default.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/feed.css" />
	<link rel="stylesheet" type="text/css" href="../CSS/sidebar2.css" />
</head>

<body>
	<div class="container">
		<div class="header">
			<a href="Feed/feed.php">Tunatic</a> 
		</div>
		<div class="sidebar1">
			<ul class="nav">
				<?php
					session_start();
					// Link para a página de perfil dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='Perfil/meu_perfil_tuna.php'>Perfil</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='Perfil/meu_perfil_utilizador.php'>Perfil</a></li>";
					}
					// Link para a página de amigos dependendo do tipo de conta que fez login
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo "<li><a href='Amigos/amigos_tuna.php'>Amigos</a></li>";
					}
					// Se a sessão for do tipo Utilizador
					else 
					{
						echo "<li><a href='Amigos/amigos_utilizador.php'>Amigos</a></li>";
					}
					include('../ligacao_bd.php');
					
					// Links para as páginas seguidores e membros de uma Tuna
					// Se a sessão for do tipo Tuna
					if($_SESSION['tipoTuna'])
					{
						echo '<li><a href="Festivais/festivais.php">Festivais</a></li>';
						echo"<li><a href='Seguidores/seguidores.php'>Seguidores</a></li>";
						echo"<li><a href='Gestao_Tuna/gerir_tuna.php'>Gestão de Tuna</a></li>";
					}
				?>
			</ul>
	    </div>
		<div class="content">
			<h3>Resultado da Pesquisa</h3>
			<?php
				// Se o campo de pesquisa não estiver vazio
				if(!empty($_POST['search']))
				{
					// ##### UTILIZADORES #####
					/* Query que selecciona as contas que contêm a(s) palavra(s) do campo de pesquisa no nome
					   onde as contas são utilizadores (is_tuna = 0) e o id da conta da sessão não seja igual 
					   ao da query (idConta NOT IN $_SESSION[idConta])*/ 
					$sql = "SELECT * FROM contas WHERE nome LIKE '%" . $_POST['search'] . "%' AND is_tuna = 0 AND idConta NOT IN (" . $_SESSION['idConta'] . ");";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo "<h3>Pessoas</h3>";
						echo "<ul class='men'>";
						while($row = mysql_fetch_array($result))
						{
							// Query que selecciona todos os utilizadores onde o id da conta é igual ao id da conta da linha actual
							$sql = "SELECT * FROM utilizadores WHERE idConta = '" . $row['idConta'] . "';";
							$result2 = mysql_query($sql, $link) or die(mysql_error($link));
							$row2 = mysql_fetch_array($result2);
							echo "<li><a href='Perfil/perfil_utilizador.php?idUtilizador=" . $row2['idUtilizador'] . "'><span style='vertical-align: -17px; padding-right: 15px;'><img src='../".$row2['img_path']."' width='50' height='50'></span>" . $row['nome'] . "</a></li>";
						}
						echo "</ul>";
					}
					
					// ##### TUNAS #####
					/* Query que selecciona as contas que contêm a(s) palavra(s) do campo de pesquisa no nome
					   onde as contas são tunas (is_tuna = 1) e o id da conta da sessão não seja igual 
					   ao da query (idConta NOT IN $_SESSION[idConta])*/ 
					$sql = "SELECT * FROM contas WHERE nome LIKE '%" . $_POST['search'] . "%' AND is_tuna = 1 AND idConta NOT IN (" . $_SESSION['idConta'] . ");";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo "<h3>Tunas</h3>";
						echo "<ul class='men'>";
						while($row = mysql_fetch_array($result))
						{
							// Query que selecciona todos as tunas onde o id da conta é igual ao id da conta da linha actual
							$sql = "SELECT * FROM tunas WHERE idConta = '" . $row['idConta'] . "';";
							$result2 = mysql_query($sql, $link) or die(mysql_error($link));
							$row2 = mysql_fetch_array($result2);
							echo "<li><a href='Perfil/perfil_tuna.php?idTuna=" . $row2['idTuna'] . "'><span style='vertical-align: -17px; padding-right: 15px;'><img src='../".$row2['img_path']."' width='50' height='50'></span>" . $row['nome'] . " | " . $row2['designacao'] . "</a></li>";
						}
						echo "</ul>";
					}
					
					// ##### MEMBROS #####
					// Query que selecciona os membros que contêm a(s) palavra(s) do campo de pesquisa na designacao
					$sql = "SELECT * FROM membros WHERE nome_tuna LIKE '%" . $_POST['search'] . "%';";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo "<h3>Membros de Tunas</h3>";
						echo "<ul class='men'>";
						while($row = mysql_fetch_array($result))
						{
							$sql = "SELECT * FROM tunas INNER JOIN contas USING (idConta) WHERE idTuna=".$row['idTuna'].";";
							$resultTuna = mysql_query($sql, $link) or die(mysql_error($link));
							$rowTuna = mysql_fetch_array($resultTuna);
							echo "<li><a href='perfil/perfil_membro_tuna.php?idMembro=".$row['idMembro']."'><span style='vertical-align: -17px; padding-right: 15px;'><img src='../".$row['img_path']."' width='50' height='50'></span>" . $row['nome_tuna'] . " | Membro de: ".$rowTuna['nome']."</a></li>";
						}
						echo "</ul>";
					}
					
					// ##### GRUPOS #####
					// Query que selecciona os grupos que contêm a(s) palavra(s) do campo de pesquisa na designacao
					$sql = "SELECT * FROM grupos WHERE designacao LIKE '%" . $_POST['search'] . "%';";
					$result = mysql_query($sql, $link) or die(mysql_error($link));
					if(mysql_num_rows($result) > 0)
					{
						echo "<h3>Grupos</h3>";
						echo "<ul class='men'>";
						while($row = mysql_fetch_array($result))
						{
							echo "<li><a href='#'>" . $row['designacao'] . "</a></li>";
						}
						echo "</ul>";
					}
				}
			?>
	    </div>
	    <div class="sidebar2">
			<?php
				if($_SESSION['tipoTuna'])
				{
					$sql = 'SELECT * FROM contas 
							INNER JOIN tunas USING (idConta)
							WHERE idTuna = ' . $_SESSION['idTuna'] . ';';
				}
				else 
				{
					$sql = 'SELECT * FROM contas 
							INNER JOIN utilizadores USING (idConta)
							WHERE idUtilizador = ' . $_SESSION['idUtilizador'] . ';';
				}
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				echo '<p><img src="../' . $row['img_path'] . '" width="150" height="150"><br /><br />Bem-Vindo ' . $row['nome'] . '</p>';
			?>
			<form action="search.php" method="post">
				<p>Pesquisa
				<input type="search" name="search" />
				<input type="submit" value="Go" /></p>
			</form>
			<ul class="nav">
			<?php
				// Se a sessão for do tipo Tuna
				if($_SESSION['tipoTuna'])
				{
					// Query que conta quantos pedidos a tuna em sessão tem
					$sql = "SELECT COUNT(*) AS numero_pedidos FROM amigos_tuna WHERE idTuna2 = " . $_SESSION['idTuna'] . " AND amigos_tuna.pedido_aceite = 0;";
					
				}
				// Se a sessão for do tipo Utilizador
				else
				{
					// Query que conta quantos pedidos o utilizador em sessão tem
					$sql = "SELECT COUNT(*) AS numero_pedidos FROM amigos_utilizador WHERE idUtilizador2 = " . $_SESSION['idUtilizador'] . " AND amigos_utilizador.pedido_aceite = 0;";
				}
				
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				echo "<li><a href='Pedidos/pedidos.php'>Pedidos (" . $row['numero_pedidos'] . ")</a></li>";
			
				// Query que conta quantas mensagens não lidas existem na caixa de entrada
				$sql = 'SELECT COUNT(*) AS nMensagens FROM mensagem_receptores WHERE idConta = ' . $_SESSION['idConta'] . ' AND lida = 0;';
				$result = mysql_query($sql, $link) or die(mysql_error($link));
				$row = mysql_fetch_array($result);
				echo "<li><a href='Mensagens/mensagens.php'>Mensagens (" . $row['nMensagens'] . ")</a></li>";
			
				echo '<li><a href="processar_logout.php">Logout</a></li>';
			
				mysql_close($link);
			?>
			</ul>
		</div>
	  	<?php
	    	include("../footer.php");
	    ?>
    </div>
</body>
</html>